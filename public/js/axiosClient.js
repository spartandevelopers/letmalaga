const Axios = axios.create({
  baseURL: 'https://185.166.213.205/',
  // baseURL: 'http://127.0.0.1/let-malaga-2.0/app/',
  timeout: 1000,
  
  // headers: {'X-Custom-Header': 'foobar'}
});

$('.axios-client').on('submit', function() {

  let form = $(this);

  let element = $(this).closest('.card');
  let request = new Object();
  request.transformRequest = [function (data, headers) {
    let formData = new FormData();
    $.each(data, function(index, val) {
      formData.append(val.name,val.value);
    });

    if (form.hasClass('summernote-form')) {
      let code = $("#summernote-area").summernote('code');
      formData.append('description',code);
    }

    return formData;
  }];


  request.onUploadProgress = (progressEvent) => {
    $(element).block({
        message: '<i class="icon-spinner10 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#1B2024',
            opacity: 0.85,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none',
            color: '#fff'
        }
    });
  }

  request.url = $(this).attr('action');
  console.log(request.url)
  request.method = $(this).attr('method');


  if (request.method == 'get') {
    request.params = $(this).serializeArray();
  }else{
    let data = $(this).serializeArray();
    request.data = data;
  }

  request = Axios(request);

  request.then((res) => {
    element.unblock();
    let response = res.data;   

    // if (response.storage) {
    //   $.each(response.storage, function(index, val) {
    //     localStorage.setItem(index, val);
    //   });
    // }
    // 
    if (response.status == 'success' || (response.type == 'redirect')) {
      if(response.message) notify(response.message,response.status);
      if (response.type == 'redirect') {
        setTimeout(() => {
          location.href = response.redirect;
        },1000);
      }
      
    }
    


    if (response.type == 'validation') {
      drawValidation(response.response);
      return;
    }

    if (response.storage.clear != null || typeof response.storage.clear != 'undefined') {
      $.each(response.storage.clear, function(index, val) {
        localStorage.removeItem(val);
      });
    }

    console.log(response.type,response.status,response.status == 'success' || (response.type == 'redirect'))

    

    if (response.type == 'icon-set') {
      let icon = response.data.icon; 

      $('.select-icon[data-id="'+response.data.id+'"]')
        .html(`<i class="${icon}"></i>`)
        .removeClass('border-grey text-grey-600')
        .addClass('border-brown text-brown-600');
    }

    if (response.status == 'error') {
      if(response.message) notify(response.message,response.status);
    }

  },(error) => {
    element.unblock();
  });

  return false;
});


let drawValidation = (response) => {
  $('.validation').text('');
  $.each(response, function(index, val) {
    $(`#validate-${index}`).text(val);
  });
}

Noty.overrideDefaults({
  theme: 'limitless',
  layout: 'topRight',
  type: 'alert',
  timeout: 2500
});

let notify = (message,type = 'success') => {
  new Noty({
    text: message,
    type: type
  }).show();
}