// $(window).scroll(function() {
//   // console.log($(window).width())
//   if ($(window).width() > 900) {
//     let element = $('.reserve .content');
//     element.css('top', 'calc(25vh + '+$(this).scrollTop()+'px)');
//   }
// });

let gallery = $(".gallery").lightGallery({
  download:false
}); 

$('.gallery-button').on('click', function() {
  console.log($('.gallery a').length)
  $('.gallery a:first-child').trigger('click');
  gallery.data($('.gallery a').length);
});

if ($('.mdc-select').length) {
  const select = new mdc.select.MDCSelect(document.querySelector('.mdc-select'));
}


// select.listen('change', () => {
//   alert(`Selected option at index ${select.selectedIndex} with value "${select.value}"`);
// });


$owl = $('.carousel');
$owl.owlCarousel({
    center: true,
    items:1,
    loop:true,
    margin:0,
    dots:false
});

$owl.on('mouseover', function() {
  $(this).trigger('play.owl.autoplay',[2000]);
});

$owl.on('mouseout', function() {
  $(this).trigger('stop.owl.autoplay');
});

$(window).on('load', function () {
  $('#loader').hide();
});

$('.faq').on('click', function() {
  $('.faq-description').removeClass('active');
  $(this).children('.faq-description').addClass('active');
});


$('#principal-form').on('submit', function(event) {
  // event.preventDefault();

  // console.log($(this).find('input[name="in"]').val(),$(this).find('input[name="out"]').val())

  if ($(this).find('input[name="in"]').val() == '' || $(this).find('input[name="out"]').val() == '') {
    Snackbar.show({
      text: 'Filtros imcompletos',
      pos: 'top-right',
      showAction: false
    });
    return false;
  }

  
});

$('input[name="filters[]"]').on('change', function() {
  let count = $('input[name="filters[]"]:checked').length;
  $('.count-filters').text(`(${count})`)
});

$('.show-filters').on('click', function() {
  $('.filters').toggleClass('hide');

  if ($('.filters').hasClass('hide')) {
    $(this).text('Mostrar filtros');
  }else{
     $(this).text('Ocultar filtros');
  }

});

(() => {

  $('.hamburger').on('click', function() {
    $(this).toggleClass('is-active');

    ($(this).hasClass('is-active')) ?  $(this).next().css('display','flex') :  $(this).next().css('display','none');
  });

  // if ($('.box.box-date.first.new').length == 0) {
  //   let picker = $('.box.box-date.first').flatpickr({
  //   mode: "range",
  //   minDate: "today",
  //   dateFormat: "d-m-Y",
  //   locale: "es",
  //   onChange: function(selectedDates, dateStr, instance) {
  //     let date = dateStr.split('a');
  //     $('.in-date').text(date[0]);
  //     $('.out-date').text(date[1]);
  //     $('input[name=in]').val(date[0]);
  //     $('input[name=out]').val(date[1]);

  //   }
  // }); 

  // $('.box.box-date.last').on('click', function() {
  //   picker.open()
  // });
  // }

  let picker = $('.dates').flatpickr({
    mode: "range",
    minDate: "today",
    dateFormat: "d-m-Y",
    locale: "es",
    onChange: function(selectedDates, dateStr, instance) {
      let date = dateStr.split('a');
      $('.in-date').text(date[0]);
      $('.out-date').text(date[1]);
      $('input[name=in]').val(date[0]);
      $('input[name=out]').val(date[1]);

    }
  })

})()