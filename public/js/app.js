var uri = $('#uri').data('value');
console.log(uri)
var DropzoneUploader = function() {
  var _componentDropzone = function() {

    if (typeof Dropzone == 'undefined') {
      console.warn('Warning - dropzone.min.js is not loaded.');
      return;
    }

    Dropzone.options.dropzoneMultiple = {
      paramName: "file", // The name that will be used to transfer the file
      dictDefaultMessage: 'Arrastra tus archivos aqui <span>o haz click</span>',
      // maxFilesize: 0.1, // MB
      // maxFilesize:
      acceptedFiles: 'image/*',
      addRemoveLinks:true,
      init: function() {


        let dropzone = this;
        let dataBanners = {type: $('#type-banner').val()};

        let classlist = [];
        for (c of dropzone.element.classList) {
          if (c == 'service' || c == 'blog') {
            dropzone.options.url += '/'+localStorage.getItem('token-image');
            dropzone.options.maxFiles = 1;
            $('input[name="token"]').val(localStorage.getItem('token-image'));
            dataBanners = {
              token: $('input[name="token"]').val()
            };

            if ($('input[name="token"]').val() != '' && parseInt(localStorage.getItem('lenght-image'))) {
              dropzone.options.maxFiles = 0;
            }

            break;
          }
        }

        this.on("removedfile", function(file) {

          if (file.data.id) {
            $.ajax({
              url: `${uri}/banners/delete/${file.data.id}`,
              type: 'get',
              dataType: 'json',
            });

            for (c of dropzone.element.classList) {
              if (c == 'service' || c == 'blog') {
                dropzone.options.maxFiles = 1;
                break;
              }
            }
          }
        });

        this.on('success', (file) =>{ 
          data = JSON.parse(file.xhr.response);
          file.data = data.data;
        })


        console.log(dropzone.options)
        $.ajax({
          url: `${uri}/banners/get/`,
          type: 'post',
          dataType: 'json',
          data: dataBanners,
          success: (res) => {
            $.each(res.data, function(key,value){
              var mockFile = { name: value.orig_name, size: value.file_size, data:value};
              dropzone.options.addedfile.call(dropzone, mockFile);
              dropzone.options.thumbnail.call(dropzone, mockFile, uri+"/public/images/banners/"+value.file_name);
            });
          }
        });

      }
    };
  };

  return {
    init: function() {
      _componentDropzone();
    }
  }
}();

var DatatableBasic = function() {
  var _componentDatatableBasic = function() {
      if (!$().DataTable) {
          console.warn('Warning - datatables.min.js is not loaded.');
          return;
      }

      // Setting datatable defaults
      $.extend( $.fn.dataTable.defaults, {
          autoWidth: false,
          columnDefs: [{ 
              orderable: false,
              width: 100,
              // targets: [ 1 ]
          }],
          dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
          language: {
              search: '<span>Filtrar:</span> _INPUT_',
              searchPlaceholder: 'Escribe tu busqueda...',
              lengthMenu: '<span>Ver:</span> _MENU_',
              paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
          }
      });

      // Basic datatable
      $('.datatable-basic').DataTable();

      // Alternative pagination
      $('.datatable-pagination').DataTable({
          pagingType: "simple",
          language: {
              paginate: {'next': $('html').attr('dir') == 'rtl' ? 'Next &larr;' : 'Next &rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr; Prev' : '&larr; Prev'}
          }
      });

      // Datatable with saving state
      $('.datatable-save-state').DataTable({
          stateSave: true
      });

      // Scrollable datatable
      var table = $('.datatable-scroll-y').DataTable({
          autoWidth: true,
          scrollY: 300
      });

      // Resize scrollable table when sidebar width changes
      $('.sidebar-control').on('click', function() {
          table.columns.adjust().draw();
      });
  };

  // Select2 for length menu styling
  var _componentSelect2 = function() {
    if (!$().select2) {
      console.warn('Warning - select2.min.js is not loaded.');
      return;
    }

    // Initialize
    $('.dataTables_length select').select2({
      minimumResultsForSearch: Infinity,
      dropdownAutoWidth: true,
      width: 'auto'
    });
  };


  return {
    init: function() {
      _componentDatatableBasic();
      _componentSelect2();
    }
  }
}();

var Summernote = function() {


  // Summernote
  var _componentSummernote = function() {
    if (!$().summernote) {
        console.warn('Warning - summernote.min.js is not loaded.');
        return;
    }

    // Basic examples
    // ------------------------------

    // Default initialization
    $('.summernote').summernote();

  };

  // Uniform
  var _componentUniform = function() {
    if (!$().uniform) {
      console.warn('Warning - uniform.min.js is not loaded.');
      return;
    }

    // Styled file input
    $('.note-image-input').uniform({
        fileButtonClass: 'action btn bg-warning-400'
    });
  };


  //
  // Return objects assigned to module
  //

  return {
      init: function() {
        _componentSummernote();
        _componentUniform();
      }
  }
}();

var InputsBasic = function () {


    //
    // Setup module components
    //

    // Uniform
    var _componentUniform = function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // File input
    $('.form-control-uniform').uniform();
        $('.form-control-uniform-custom').uniform({
            fileButtonClass: 'action btn bg-blue',
            selectClass: 'uniform-select bg-pink-400 border-pink-400'
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentUniform();
        }
    }
}();


document.addEventListener('DOMContentLoaded', function() {
  DatatableBasic.init();
  Summernote.init();
  InputsBasic.init();
});


DropzoneUploader.init();

$('.select-icon').on('click', function() {
  $('#icons-modal').modal();
  $('input[name="addon"]').val($(this).data('id'));
});

$('.icon-grids .icon').on('dblclick', function() {

  $('.icon-grids .icon').removeClass('selected');
  $(this).addClass('selected');

  let icon = $(this).children('i');
  
  $('input[name="icon"]').val(icon.attr('class'));
  $('#icons-modal').modal('hide');
  $('#icon-form').submit();

});

