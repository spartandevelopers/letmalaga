<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DescriptionModel extends CI_Model {

  private $table = 'descriptions';
  private $table_pivot = 'descriptions_properties';

  function get($property_id)
  {

   $descriptions_temp = $this->db->get_where($this->table_pivot,[
    'property_id' => $property_id
   ]); 

   if (!$descriptions_temp->num_rows()) {
    return [];
   }

   $descriptions = [];

   foreach ($descriptions_temp->result() as $item) {
     $descriptions[] = $this->db->get_where($this->table,[
      'id' => $item->description_id
     ])->result()[0];
   }

   return $descriptions;

   
  }

}

/* End of file DescriptionModel.php */
/* Location: ./application/models/DescriptionModel.php */