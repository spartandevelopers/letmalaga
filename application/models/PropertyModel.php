<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'/models/SuperModel.php';

class PropertyModel extends SuperModel {

  private $table = 'properties';

  function get($page = 'properties',$data = false)
  {

    $query = $this->construct_query($this->table,$data);

    $properties = $query->result();
    foreach ($properties as $property) {
      $property->images = $this->get_images($property,$page);
    }
    
    return $properties;
  }

  function get_witch_limit($omit = 0,$limit = 133){
    if ($omit != 0) {
      $this->db->limit($omit,$limit);
    }else{
      $this->db->limit($limit);
    }
    $properties = $this->db->get('properties')->result();
    foreach ($properties as $property) {
      $property->images = $this->get_images($property);
      $property->addons = $this->addon->get_addons_id_by_property($property);
    }
    return $properties;

  }

  function get_filter($ids,$omit,$limit = 6){
    $query = $this->db->limit($limit,$omit)->where_in('id', $ids)->get($this->table);

    $properties = $query->result();
    foreach ($properties as $property) {
      $property->images = $this->get_images($property,'properties');
    }
    
    return $properties;

  }

  function get_images($property){
    $images = $this->image->get($property->id,'properties');
    $images[] = ['principal' => true, 'image' => $property->image];

    return $images;
  }

}

/* End of file PropertyModel.php */
/* Location: ./application/models/PropertyModel.php */