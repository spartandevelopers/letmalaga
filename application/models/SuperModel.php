<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuperModel extends CI_Model {

  private $table;

  function construct_query($table,$data)
  {

    $this->table = $table;


    if (isset($data['limit']) and !isset($data['rand'])) {
      $this->db->limit($data['limit']);
    }


    if (isset($data['order'])) {
      $this->db->order_by($data['order']['key'],$data['order']['type']);
    }

    if (isset($data['query'])) {
      $query = $this->db->get_where($this->table,$data['query']);
    }else{
      $query = $this->db->get($this->table);
    }

    if (isset($data['rand']) and $data['rand']) {
      $this->db->reset_query();
      $query = $this->db->get($this->table)->result();

      if (isset($data['limit'])) {
        $number = $data['limit'];
      }else{
        $number = 2;
      }

      foreach ($query as $item) {
        $ids[] = $item->id;
      }


      foreach (array_rand($ids,$number) as $value) {
        $items[] = $query[$value]->id;
      }      
     
      $query = $this->db->where_in('id',$items)->get($this->table);    
 
    }

    return $query;
  }

}

/* End of file SuperModel.php */
/* Location: ./application/models/SuperModel.php */