<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ImageModel extends CI_Model {

  private $table = 'images';

  function get($property_id,$page)
  {
    $params = [];

    $size = '350x262';
    $params['size'] = $size;
    $params = array_merge(['property_id' => $property_id],$params);
    $query = $this->db->get_where($this->table,$params);

    if (!$query->num_rows()) {
      return [];
    }

    foreach ($query->result() as $image) {
      $images[] = ['size' => $size,'image' => $image->image];
    }

    return $images;

    if ($page == 'home') {
      $size = '350x262';
      $params['size'] = $size;
      $params = array_merge(['property_id' => $property_id],$params);
      $query = $this->db->get_where($this->table,$params);

      if (!$query->num_rows()) {
        return [];
      }

      foreach ($query->result() as $image) {
        $images[] = ['size' => $size,'image' => $image->image];
      }

    }elseif ($page == 'property') {
      $sizes_keys = ['350x262','700x525'];
      
      foreach ($sizes_keys as $size) {
        $query = $this->db->get_where($this->table,[
          'size' => $size
        ])->result();

        foreach ($query as $image) {
          $images[$image->size][] = ['size' => $image->size,'image' => $image->image];
        }
      }

    }

    return $images;

  }

}

/* End of file ImageModel.php */
/* Location: ./application/models/ImageModel.php */