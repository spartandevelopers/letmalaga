<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends SuperModel {

  private $table = 'users';

  function get($data = false)
  {

    $query = $this->construct_query($this->table,$data);
    
    return $query;
  }

}

/* End of file UserModel.php */
/* Location: ./application/models/UserModel.php */