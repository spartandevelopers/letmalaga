<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'/models/SuperModel.php';
class BannerModel extends SuperModel {

  private $table = 'banners';

  function get($data = false)
  {
    $query = $this->construct_query($this->table,$data);
    // if (!$query->num_rows()) {
    //   return
    // }
    $banners = $query->result();
    return $banners;
   
  }

  function delete($id)
  {
    $banner = $this->get([
      'query' => [
        'id' => $id
      ]
    ])[0];
    unlink('./public/images/banners/'.$banner->file_name);

    $this->db->where('id', $id);
    $this->db->delete($this->table);
  }

}

/* End of file BannerModel.php */
/* Location: ./application/models/BannerModel.php */