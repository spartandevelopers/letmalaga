<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'/models/SuperModel.php';
class BlogModel extends SuperModel {

  private $table = 'posts';

  function get($data = false)
  {
    $query = $this->construct_query($this->table,$data);
    $posts = $query->result();

    foreach ($posts as $post) {
      $post = $this->get_info($post);
    }

    return $posts;
   
  }

  function get_info($post){
    $post->image = $this->banner->get([
      'query' => [
        'token' => $post->token
      ]
    ]);

    if (count($post->image)) {
       $post->image =  $post->image[0];
    }

    $post->category = $this->db->get_where('categories_blog',[
      'id' => $post->category
    ])->result()[0];

    return $post;
  }

}

/* End of file BlogModel.php */
/* Location: ./application/models/BlogModel.php */