<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddonModel extends CI_Model {

  private $table = 'addons';
  private $table_pivot = 'addons_properties';

  function get($property_id,$type)
  {

   $addons_temp = $this->db->get_where($this->table_pivot,[
    'property_id' => $property_id
   ]); 

   if (!$addons_temp->num_rows()) {
    return [];
   }

   $addons = [];

   foreach ($addons_temp->result() as $item) {
     $query= $this->db->get_where($this->table,[
      'id' => $item->equipment_id,
      'type' => $type
     ]);

     if ($query->num_rows()) {
       $addons[] = $query->result()[0];
     }

   }

   return $addons;

   
  }

  function get_all()
  {
    return $this->db->get($this->table)->result();
  }

  function get_by_type($type){
    return $this->db->get_where($this->table,[
      'type' => $type
    ])->result();
  }

  function get_addons_id_by_property($property){
    $query = $this->db->get_where('addons_properties',[
      'property_id' => $property->id
    ])->result();
    $ids = [];
    foreach ($query as $q) {
      $ids[] = $q->equipment_id; 
    }
    if (count($ids)) {
      $query = $this->db->where('type','equipment')->where_in('id', $ids)->get($this->table)->result();
      $addons = [];
      foreach ($query as $q) {
        $addons[] = $q->id;
      }

     return $addons;
    }

    return [];
    

  }

  function get_principals($keys){
    return $this->db->where('type','equipment')->where_in('name', $keys)->get($this->table)->result();
  }

  function set()
  {
    $add = $this->db->get_where($this->table,[
      'id' => $this->input->post('addon')
    ])->result()[0];

    $add->icon = $this->input->post('icon');
    $this->db->replace($this->table, $add);
    return $add;
  }

}

/* End of file AddonModel.php */
/* Location: ./application/models/AddonModel.php */