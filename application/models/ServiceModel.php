<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceModel extends SuperModel {

  private $table = 'services';

  function get($data = false)
  {
    $query = $this->construct_query($this->table,$data);
    $services = $query->result();

    foreach ($services as $service) {
      $service->image = $this->banner->get([
        'query' => [
          'token' => $service->token
        ]
      ]);

      if (count($service->image)) {
         $service->image =  $service->image[0];
      }
    }

    return $services;
   
  }

}

/* End of file ServiceModel.php */
/* Location: ./application/models/ServiceModel.php */