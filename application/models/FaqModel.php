<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'/models/SuperModel.php';
class FaqModel extends SuperModel {

  private $table = 'faqs';

  function get($data = false)
  {
    $query = $this->construct_query($this->table,$data);
    
    $banners = $query->result();
    return $banners;
   
  }

}

/* End of file FaqModel.php */
/* Location: ./application/models/FaqModel.php */