<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/controllers/Api/ApiController.php';
class ServicesController extends ApiController {

  public function index()
  {
    $services = $this->service->get();
    $posts = $this->blog->get();

   

    $this->load->view('panel/services/services',[
      'services' => $services,
      'posts' => $posts 
    ]); 
  }

  function create()
  {
    $data = [];
    if ($this->input->get('type') == 'post') {
      $data['categories'] = $this->db->get('categories_blog')->result();
    }

    $this->load->view('panel/services/create',$data);
  }

  function store($type = false){
    $rules = [
      [
        'field' => 'title',
        'label' => 'Titulo',
        'rules' => 'trim|required'
      ],
      [
        'field' => 'description',
        'label' => 'Descripcion',
        'rules' => 'trim|required'
      ]

    ];

    if (!$this->validate($rules)) return;

    if (!$type) {
      $table = 'services'; 
    }else{
      $table = $type;
    }
    
    $this->save($this->input->post(),$table);

    $this->response([
      'storage' => [
        'clear' => ['token-image']
      ],
      'status' => 'success',
      'type' => 'redirect',
      'redirect' => base_url('panel/services')
    ]);
  }

  function edit($id)
  {

    if ($this->input->get('type') != 'post') {
       $data = $this->service->get([
        'query' => [
          'id' => $id
        ]
      ])[0];
    }else{
      $data = $this->blog->get([
        'query' => [
          'id' => $id
        ]
      ])[0];
    }

    $data = [
      'service' => $data
    ];

    if ($this->input->get('type') == 'post') {
      $data['categories'] = $this->db->get('categories_blog')->result();
    }
   

    $this->load->view('panel/services/edit',$data);
  }

  function banner_store($token)
  {

    $config['upload_path']          = './public/images/banners/';
    $config['allowed_types']        = '*';
    $config['encrypt_name'] = true;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('file')) {

      $this->response([
        'status' => 'error',
        'message' => 'falla al procesar la imagen',
        'data' => $this->upload->display_errors()
      ]);
     return;

    }else{

      $file = $this->upload->data();

      foreach (['file_path','full_path','raw_name','client_name','is_image','image_size_str'] as $remove) {
        unset($file[$remove]);
      }

      $file['type'] = 'services';
      $file['token'] = $token;

      $this->save($file,'banners');

      $this->response([
        'data' => $this->banner->get([
          'query' => [
            'id' => $this->db->insert_id()
          ]
        ])[0]
      ]);

    }
  }

  function update($type = false){
    $rules = [
      [
        'field' => 'title',
        'label' => 'Titulo',
        'rules' => 'trim|required'
      ],
      [
        'field' => 'description',
        'label' => 'Descripcion',
        'rules' => 'trim|required'
      ]

    ];

    if (!$this->validate($rules)) return;

    if (!$type) {
      $table = 'services'; 
    }else{
      $table = $type;
    }
    
    $this->update_reg($this->input->post(),$table);

    $this->response([
      'storage' => [
        'clear' => ['token-image']
      ],
      'status' => 'success',
      'type' => 'redirect',
      'redirect' => base_url('panel/services')
    ]);

    
  }

  function delete($id){

    $table = ($this->input->get('type') == 'post') ? 'posts' : 'services';

    if ($this->input->get('type') == 'post') {
      $data = $this->blog->get(['id' => $id])[0]; 
    }else{
      $data = $this->service->get(['id' => $id])[0]; 
    }   

    $this->db->delete('banners',['token' => $data->token]);
    $this->db->delete($table,['id' => $id]);
    redirect('panel/services','refresh');
  }

}

/* End of file ServicesController.php */
/* Location: ./application/controllers/ServicesController.php */