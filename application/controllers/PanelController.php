<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PanelController extends CI_Controller {


  public function __construct()
  {
    parent::__construct();
    if (!isset($this->session->login) or !$this->session->login) {
      redirect('panel/login','refresh');
    }
  }

  public function index()
  {
    $this->load->view('panel/home');
  }

}

/* End of file PanelController.php */
/* Location: ./application/controllers/PanelController.php */