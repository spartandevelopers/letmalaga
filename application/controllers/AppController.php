<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/controllers/Api/SoapClientController.php';
class AppController extends SoapClientController {

  function no_found()
  {
    echo 'En construccion';
  }

  public function index()
  {

    $services = $this->service->get([
      'limit' => 6,
      'order' => [
        'key' => 'id',
        'type' => 'des'
      ]
    ]);


    $properties = $this->property->get('home',[
      'limit' => 3,
      'order' => [
        'key' => 'rating',
        'type' => 'asc'
      ]
    ]);

    $posts = $this->blog->get([
      'order' => [
        'key' => 'id',
        'type' => 'des'
      ],
      'limit' => 2
    ]);

    $this->load->view('home',[
      'properties' => $properties,
      'services' => $services,
      'posts' => $posts,
      'background' => false
    ]);
  }

  function properties()
  {

    $properties = $this->property->get('properties',[
      'limit' => 6
    ]);

    

    $this->load->view('search',[
      'properties' => $properties
    ]);
  }

  function property($name)
  {



    $count = 1;
    $name = str_replace('-',' ',$name);

    $property = $this->property->get('property',['query' => ['name' => $name]])[0];
    $calendar = $this->call('icneaXlodgingXcalendar',[
      'lodging' => $property->id,
      'first_date' => date('Y-m-d'),
      'number_of_months' => 12
    ]);

    $disables_dates = [];

    foreach ($calendar as $month) {
      $disabled[0] = null;
      $disabled[1] = $month->month; 
      $disabled[2] = $month->year;

      foreach ($month->availability as $day) {
        if ($day->available != 'True') {
          $disabled[0] = $day->day;
          $disables_dates[] = implode('-',$disabled);
        }
      }

      
    }

    

    $property->descriptions = $this->description->get($property->id);
    $property->addons = $this->addon->get($property->id,'equipment');

    $alternative_properties = $this->property->get('property',[
      'limit' => 2,
      'rand' => true
    ]); 

   

    $this->load->view('properties/property',[
      'property' => $property,
      'alternative_properties' => $alternative_properties,
      'disables_dates' => json_encode($disables_dates)
    ]);

  }

  function services(){

    $services = $this->service->get([
      'order' => [
        'key' => 'id',
        'type' => 'des'
      ]
    ]);

    $this->load->view('services/services',[
      'services' => $services,
      'type' => 'services'
    ]);
  }

  function service($id)
  {

    $service = $this->service->get([
      'query' => [
        'id' => $id
      ]
    ])[0];
    $alternative_properties = $this->property->get('property',[
      'limit' => 2
    ]); 

    $this->load->view('services/service',[
      'service' => $service,
      'alternative_properties' => $alternative_properties,
      'type' => 'services'
    ]);


  }

  function blog()
  {
    $post = $this->blog->get([
      'order' => [
        'key' => 'id',
        'type' => 'des'
      ]
    ]);

    $this->load->view('services/services',[
      'services' => $post,
      'type' => 'article'
    ]);
  }

  function article($id)
  {

    $service = $this->blog->get([
      'query' => [
        'id' => $id
      ]
    ])[0];
    $alternative_properties = $this->property->get('property',[
      'limit' => 2,
      'rand' => true
    ]); 


    $this->load->view('services/service',[
      'service' => $service,
      'type' => 'blog',
      'alternative_properties' => $alternative_properties
    ]);


  }

  function accept_cookie(){
    $array = array(
      'cookies' => true
    );
    
    $this->session->set_userdata( $array );
  }

  function contact(){
    $this->load->view('contact');
  }

  function about(){
     $this->load->view('about');
  }

  function privacity(){
     $this->load->view('privacity');
  }

  function cookies(){
     $this->load->view('cookies');
  }

  function faqs(){
    $this->load->view('faqs',[
      'faqs' => $this->faq->get()
    ]);
  }

}

/* End of file AppController.php */
/* Location: ./application/controllers/AppController.php */