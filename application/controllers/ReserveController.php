<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReserveController extends CI_Controller {

  public function index()
  {
    foreach ($this->input->get() as $key => $value) {
      if ($key == 'in' || $key == 'out') {
        $value = str_replace('-','/',$value);
      }
      $params[$key] = trim($value);
    }
   

    $iframe = "https://tpv.icnea.net/Reserva.aspx?idi=3&hot=".$params['hot']."&emp=1552&ent=".$params['in']."&sor=".$params['out']."&per=".$params['persons'];

    // $iframe = 'https://tpv.icnea.net/Reserva.aspx?idi=2&hot=2334&emp=1552&ent=05/12/2018&sor=09/12/2018&per=2';

    // echo "<pre>";
    // print_r ($iframe);
    // echo "</pre>";

    $this->load->view('reserve',[
      'iframe' => $iframe,
      'property' => $this->property->get('reserve',$params['hot'])[0]
    ]);

  }

}

/* End of file ReserveController.php */
/* Location: ./application/controllers/ReserveController.php */