<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/controllers/Api/SoapClientController.php';
class FilterController extends SoapClientController {

  public $filters = [];

  public function __construct()
  {
    parent::__construct();

   
    $this->original_filters = $this->input->get();

    foreach ($this->original_filters as $key => $param) {
      if ($key == 'omit') {
        unset($this->original_filters[$key]);
        // break;
      }
    }
    foreach ($this->input->get() as $key => $param) {
      if ($key == 'in' or $key == 'out') {
        $param = explode('-',$param);
        $param = array_reverse($param);

        foreach ($param as $value) {
           $this->filters[$key][] = trim($value);
        }
      }else{
        $this->filters[$key] = $param;
      }
    }

  
  }

  public function index($omit = 0)
  {

   
    if ($this->input->get('in') == '' and $this->input->get('out') == '') {
      $ids = $properties = $this->property->get_witch_limit($omit,133);
    }else{

      if (isset($this->filters['in'])) {
         $in = implode('-',$this->filters['in']);
      }else{
        $in = date('Y-m-d');
      }

      if (isset($this->filters['out'])) {
         $out = implode('-',$this->filters['out']);
      }else{
        $out = date('Y-m-d');
      }

      $ids = null;
     
      

      if (isset($this->filters['in']) and isset($this->filters['out'])) {
        $datetime1 = new DateTime($in);
        $datetime2 = new DateTime($out);

        $interval = $datetime1->diff($datetime2)->d;

        $data = $this->call('icneaXsearchXfilter',[
          'arrival_day' => $in,
          'nights' => $interval,
          'people' => 1,
          'price' => 0,
          'region' => 0,
          'type' => 21,
          'number_of_rooms' => 1,
          'number_of_bathrooms' => 1,
          'search_filters' => ''
        ]);


        $ids = [];
        foreach ($data as $property) {
         $ids[] = $property->lodging_id;
        }

       

        if ($this->input->get('omit')) {
          $omit = $this->input->get('omit');
        }
        $properties = $this->property->get_filter($ids,$omit,6);
      }else{
        $ids = $properties = $this->property->get_witch_limit($omit,133);
      }

    }

    if (count($this->input->get('filters'))) {
      $properties_filter = [];

      foreach ($properties as $p) {
        $c = count(array_intersect($p->addons,$this->input->get('filters')));

        if ($c == count($this->input->get('filters'))) {
          $properties_filter[] = $p;
        }
      }
      $ids = $properties = $properties_filter;
    }

    $keys_addons = [
      'Aire acondicionado',
      'Lavadora',
      'Cocina',
      'Parking',
      'WiFi',
      'Tv satélite',
      'Teléfono',
      'Equipo de música'
    ];
    
    $addons = $this->addon->get_principals($keys_addons); 
    // $addons = $this->addon->get_by_type('equipment');  
   
  
    $config['base_url'] = base_url('filter?').http_build_query($this->original_filters);
    $config['total_rows'] = count($ids);
    $config['per_page'] = 6;
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
    // $config['next_tag_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tag_close']  = '</span></li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tag_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tag_close']  = '</span></li>';
    $config['enable_query_strings'] = true;
    $config['page_query_string'] = true;
    $config['query_string_segment'] = 'omit';

    $this->pagination->initialize($config);

    $this->load->view('search',[
      'properties' => $properties,
      'counter' => $config['total_rows'],
      'paginate' => $this->pagination->create_links(),
      'addons' => $addons
    ]);
    
    

 
  }

}

/* End of file FilterController.php */
/* Location: ./application/controllers/FilterController.php */