<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/controllers/Api/ApiController.php';
class FaqsController extends ApiController {

  public function index()
  {
    $faqs = $this->faq->get();

    $this->load->view('panel/faqs/faqs',[
      'faqs' => $faqs
    ]);
  }

  function create(){
    $this->load->view('panel/faqs/create');
  }

  function store(){
    $rules = [
      [
        'field' => 'answer',
        'label' => 'Pregunta',
        'rules' => 'trim|required'
      ],
      [
        'field' => 'description',
        'label' => 'Descripcion',
        'rules' => 'trim|required'
      ]

    ];

    if (!$this->validate($rules)) return;

    $data = $this->input->post();
    $data['response'] = $data['description'];
    unset($data['description']);
    unset($data['token']);
    $this->save($data,'faqs');

    $this->response([
      'status' => 'success',
      'type' => 'redirect',
      'redirect' => base_url('panel/faqs')
    ]);
  }

  function edit($id){
    $this->load->view('panel/faqs/edit',[
      'faq' => $this->faq->get([
        'query' => [
          'id' => $id
        ]
      ])[0]
    ]);
  }

  function update($type = false){
    $rules = [
      [
        'field' => 'answer',
        'label' => 'Pregunta',
        'rules' => 'trim|required'
      ],
      [
        'field' => 'description',
        'label' => 'Descripcion',
        'rules' => 'trim|required'
      ]

    ];

    if (!$this->validate($rules)) return;

    

    $data = $this->input->post();

    $data['response'] = $data['description'];
    unset($data['description']);
    
    $this->update_reg($data,'faqs');

    $this->response([
      'status' => 'success',
      'type' => 'redirect',
      'redirect' => base_url('panel/faqs')
    ]);

    
  }

  function delete($id){


    $this->db->delete('faqs',['id' => $id]);
    redirect('panel/faqs','refresh');
  }

}

/* End of file FaqsController.php */
/* Location: ./application/controllers/FaqsController.php */