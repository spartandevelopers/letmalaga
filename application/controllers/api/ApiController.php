<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiController extends CI_Controller {

  function response($response)
  {
    $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        // ->_display();
    // exit();
  }

  function save($data = false,$table = false)
  {
    if (!$data || !$table) {
      return false;
    }

    if (!$this->db->insert($table, $data)) {

      $this->logModel->save([
        'status' => 'error',
        'type' => 'database',
        'message' => $this->db->error()
      ]);


      return false;
    }

    return true;
  }

  function update_reg($data = false,$table = false){
    $this->db->replace($table, $data);
    return true;
  }

  function validate($rules)
  {
    $this->form_validation->set_rules($rules);

    if (!$this->form_validation->run()) {
      return $this->response([
        'status' => 'error',
        'type' => 'validation',
        'response' => $this->form_validation->error_array() 
      ]);
    }

    return true;
  }

}

/* End of file ApiController.php */
/* Location: ./application/controllers/api/ApiController.php */