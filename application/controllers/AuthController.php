<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/controllers/Api/ApiController.php';
class AuthController extends ApiController {

  function login()
  {
    $this->load->view('auth/login');
  }

  function auth()
  {
    $rules = [
      [
        'field' => 'email',
        'label' => 'Email',
        'rules' => 'trim|required|valid_email'
      ],
      [
        'field' => 'password',
        'label' => 'Contraseña',
        'rules' => 'trim|required'
      ]

    ];

    if (!$this->validate($rules)) return;

    $user = $this->user->get([
      'query' => [
        'email' => $this->input->post('email'),
        'password' => $this->input->post('password')
      ]
    ]);

   if (!$user->num_rows()) {
     $this->response([
        'status' => 'error',
        'message' => 'Verifique sus datos'
      ]);
     return;
   }

   $user = $user->result()[0];

    
    $this->session->set_userdata([
      'email' => $user->email,
      'role' => $user->role,
      'login' => true
    ]);

    $this->response([
      'storage' => [
        'login' => true
      ],
      'status' => 'success',
      'type' => 'redirect',
      'redirect' => base_url('panel')
    ]);
  }

  function logout(){

    $this->session->sess_destroy();
    redirect('/','refresh');
  }

}

/* End of file AuthController.php */
/* Location: .//C/xampp/htdocs/let-malaga-2.0/app/AuthController.php */