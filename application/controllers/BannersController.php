<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/controllers/Api/ApiController.php';
class BannersController extends ApiController {

  public function index($type)
  {

    $this->load->view('panel/banners/banners',[
      'type' => $type
    ]);

  }

  function store()
  {
    $config['upload_path']          = './public/images/banners/';
    $config['allowed_types']        = '*';
    $config['encrypt_name'] = true;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('file')) {

      $this->response([
        'status' => 'error',
        'message' => 'falla al procesar la imagen',
        'data' => $this->upload->display_errors()
      ]);
     return;

    }else{

      $file = $this->upload->data();

      foreach (['file_path','full_path','raw_name','client_name','is_image','image_size_str'] as $remove) {
        unset($file[$remove]);
      }

      $file['type'] = $this->input->post('type');

      $this->save($file,'banners');

      $this->response([
        'data' => $this->banner->get([
          'query' => [
            'id' => $this->db->insert_id()
          ]
        ])[0]
      ]);

    }
  }

  function get()
  {

    $query = $this->input->post();

    $banners = $this->banner->get([
      'query' => $query
    ]);
    
    $this->response([
      'data' => $banners
    ]);

  }

  function delete($id)
  {

    $this->banner->delete($id);

  }

}

/* End of file BannersController.php */
/* Location: ./application/controllers/BannersController.php */