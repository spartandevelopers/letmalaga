<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/controllers/Api/ApiController.php';
class IconsController extends ApiController {

  public function index()
  {
    $addons = $this->addon->get_all();

    $this->load->view('panel/icons/icons',[
      'addons' => $addons
    ]);
  }

  function set()
  {
    
    $this->response([
      'status' => 'success',
      'message' => 'Icono actualizado',
      'data' => $this->addon->set(),
      'type' => 'icon-set'
    ]);
  }

}

/* End of file IconsController.php */
/* Location: ./application/controllers/IconsController.php */