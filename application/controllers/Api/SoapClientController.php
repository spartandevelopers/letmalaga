<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SoapClientController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('Nusoap_lib');
    $this->credentials = [
      'usr' => 1552,
      'pwd' => 'O92BRzt16r4601sU'
    ];

    $this->format = 'json';

    $this->autoload_data = array_merge($this->credentials, [
      'language' => 'es',
      'format' => $this->format
    ]);
  }

  function change_format($format)
  {
    // $this->format = $format;
    $this->autoload_data = array_merge($this->credentials, [
      'language' => 'es',
      'format' => $format
    ]);
  }

  function generate()
  {
    $client = new nusoap_client('https://ws.icnea.net/WSwebs.asmx?wsdl','wsdl');
    $err = $client->getError();

    if ($err) {
      $c = ([
        'status' => false,
        'message' => 'Client Error'. $err
      ]);
    }else{
      $c = [
        'status' => true,
        'client' => $client
      ];
    }

    $client = ($c['status']) ? $c['client'] : false;

    if ($client) {
      return $client;
    }
    
  }

  function response($data)
  {
    return (array) $this->verifyJson($data);
    foreach ($this->verifyJson($data) as $value) {
      return $value;
      break;
    }
  }

  /**
   * Format Data
   */

  function utf8ize($d) {

    if (is_array($d)) {
        foreach ($d as $k => $v) {
            $d[$k] = $this->utf8ize($v);
        }
    } else if (is_string ($d)) {
        return utf8_encode($d);
    }
    return $d;
  }


  function verifyJson($str){

    $data = json_decode($this->utf8ize($str));

    switch(json_last_error()) {
        case JSON_ERROR_NONE:
            $data = $data;
        break;
        case JSON_ERROR_DEPTH:
            $data = ' - Excedido tamaño máximo de la pila';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            $data = ' - Desbordamiento de buffer o los modos no coinciden';
        break;
        case JSON_ERROR_CTRL_CHAR:
            $data = ' - Encontrado carácter de control no esperado';
        break;
        case JSON_ERROR_SYNTAX:
            $data = null;
        break;
        case JSON_ERROR_UTF8:
            $data = ' - Caracteres UTF-8 malformados, posiblemente están mal codificados';
        break;
        default:
            $data = $data;
        break;
    }

    return $data;
  }

  function xml($str)
  {
    $xml = new SimpleXMLElement($this->utf8ize($str));
    return $xml;
  }

  public function call($action = 'icneaXcatalog',$params = [])
  {
    $client = $this->generate();
    $params = array_merge($this->autoload_data, $params);



    if ($this->autoload_data['format'] == 'json') {

      return $this->response($client->call($action, $params)[$action."Result"])[$action];
    }

    if (isset($client->call($action, $params)[$action."Result"])) {
      $xml = $client->call($action, $params)[$action."Result"];
      return $this->xml($xml);
    }

    return false;
    
  }

}

/* End of file SoapClientController.php */
/* Location: ./application/controllers/client/SoapClientController.php */