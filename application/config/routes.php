<?php
defined('BASEPATH') OR exit('No direct script access allowed');



$route = [
 'default_controller' => 'AppController',
 '404_override' => 'AppController/no_found',
 // '404_override' => '',
 'translate_uri_dashes' => FALSE,

 'properties' => 'FilterController',
 'property/(:any)' => 'AppController/property/$1',

 'services' => 'AppController/services',
 'services/(:num)' => 'AppController/service/$1',


 'blog' => 'AppController/blog',
 'article/(:num)' => 'AppController/article/$1',

 'contact' => 'AppController/contact',
 'about' => 'AppController/about',

 'privacity' => 'AppController/privacity',
 'cookies' => 'AppController/cookies',
 'faqs' => 'AppController/faqs',

 'accept_cookie' => 'AppController/accept_cookie',

 /** filter */

 'filter' => 'FilterController',
 'filter/(:num)' => 'FilterController/index/$1',

 /** Control Panel */

 'panel' => 'PanelController',


 /** AUTH */

 'panel/login' => 'AuthController/login',
 'panel/auth' => 'AuthController/auth',
 'panel/logout' => 'AuthController/logout',


 /** Banners */

 'panel/banners/index/(:any)' => 'BannersController/index/$1',
 'panel/banners/store' => 'BannersController/store',

 'banners/get' => 'BannersController/get',
 'banners/delete/(:num)' => 'BannersController/delete/$1',

 /** Services */

 'panel/services' => 'ServicesController',
 'panel/services/create' => 'ServicesController/create',
 'panel/services/store' => 'ServicesController/store',
 'panel/services/banner/store/(:any)' => 'ServicesController/banner_store/$1',
 'panel/services/edit/(:num)' => 'ServicesController/edit/$1',
 'panel/services/update' => 'ServicesController/update',
 'panel/services/delete/(:num)' => 'ServicesController/delete/$1',

 /** Blog */

 'panel/blog/post/create' => 'ServicesController/create',
 'panel/blog/post/store' => 'ServicesController/store/posts',
 'panel/posts/edit/(:num)' => 'ServicesController/edit/$1',
 'panel/blog/post/update' => 'ServicesController/update/posts',

 /** Faqs */

 'panel/faqs' => 'FaqsController',
 'panel/faqs/create' => 'FaqsController/create',
 'panel/faqs/store' => 'FaqsController/store',
 'panel/faqs/edit/(:num)' => 'FaqsController/edit/$1',
 'panel/faqs/update' => 'FaqsController/update',

 'panel/faqs/delete/(:num)' => 'FaqsController/delete/$1',

 /** icons */


 'panel/icons' => 'IconsController',
 'panel/icons/set' => 'IconsController/set',

 /** Reserve */

 'reserve' => 'ReserveController'

];
