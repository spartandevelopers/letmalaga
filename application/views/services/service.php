<?php $this->load->view('partials/header'); ?>

<section id="content-page">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="breadcrumb-letmalaga">
          <a href="<?=base_url($type)?>" class="return-back text-uppercase">Volver al listado</a>
        </div>
      </div>
      <div class="col-lg-8">
        
        <div class="property">
          <div class="banner" style="background-image: url('<?=base_url('public/images/banners')?>/<?=$service->image->file_name?>')"></div>
          <div class="info-property-principal">
            <div class="name"><?=$service->title?></div>
          </div>
          <div class="info-property">
            <div class="section">
              <div class="section-title text-uppercase">Datos del servicio</div>
              <div class="section-content">
                <?=$service->description?>
              </div>
            </div>

            
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="section-title" style="margin-top: 0">Mira estas propiedades</div>
        <div class="properties">
          <div class="row">
            <?php foreach ($alternative_properties as $property): ?>
              <?php $this->load->view('properties/property-component',[
                'property' => $property,
                'grid' => 'col-12'
              ]); ?>
            <?php endforeach ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php $this->load->view('partials/footer'); ?>

