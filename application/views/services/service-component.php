<div class="col-lg-6">
  <a href="<?=base_url($type.'/'.$service_blog->id)?>" class="item-service-blog">
    <div class="owl-carousel owl-theme carousel">
      <div class="item">
        
        <?php if (isset($service_blog->image->file_name)): ?>
          <div class="mini-banner" style="background-image: url('<?=base_url('public/images/banners')?>/<?=$service_blog->image->file_name?>')"></div>
        <?php else: ?>
          <div class="mini-banner" style="background-image: url('')"></div>
        <?php endif ?>
      </div>
    </div>

    <div class="info-property">
      <div class="property-title"><?=$service_blog->title?></div>
    </div>
  </a>
</div>