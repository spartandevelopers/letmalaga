<?php $this->load->view('partials/header'); ?>

<section id="content-page" class="search-bg">
  <div class="container">
   <div class="services-blog">
      <div class="row">

        <div class="col-12">
          <div class="section-title">Nuestros servicios</div>
          <div class="section-subtitle">Todo lo que Letmalaga puede ofrecerte para hacer que tu experiencia sea un sueño</div>
        </div>
        <div class="col-lg-8">
          <div class="row">
            <?php foreach ($services as $service): ?>
              <?php $this->load->view('services/service-component',[
                'service_blog' => $service,
                'type' => $type
              ]); ?>
            <?php endforeach ?>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="row">
            <div class="col-12">
              <?php
                $banner = $this->banner->get(['type' => 'ads'])[0];
              ?>
              <div class="banner-vertical" style="background-image: url(<?=base_url('public/images/banners/'.$banner->file_name)?>)">
                
              </div>
            </div>
          </div>
          
        </div>
        
      </div>        
    </div>
  </div>
</section>



<?php $this->load->view('partials/footer'); ?>