<?php $this->load->view('partials/header'); ?>


<section class="content-page">

  <div class="billboard">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="billboard-title">Contacto</div>
        </div>
      </div>
    </div>
    
  </div>
  
  <div class="container">
    

      <div class="row">
        <div class="col-lg-8">
          <div id="map-horizontal"></div>
        </div>

        <div class="col-lg-4">
          <?php $this->load->view('contact-form'); ?>
        </div>
        
      </div>        
    

  </div>

</section>


<?php $this->load->view('partials/footer'); ?>

 <script>

    var myIcon = L.icon({
      iconUrl: 'https://image.flaticon.com/icons/png/512/33/33622.png',
      iconSize: [30, 30],
      iconAnchor: [22, 94],
      popupAnchor: [-3, -76],
      shadowUrl: 'my-icon-shadow.png',
      shadowSize: [68, 95],
      shadowAnchor: [22, 94]
    });

    var mymap = L.map('map-horizontal').setView([44.505, -0.09], 13);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 18,
     
      id: 'mapbox.streets'
    }).addTo(mymap);

    let marker = L.marker([44.505, -0.09]).addTo(mymap);
    

  </script>