<div class="contact-form">
  <div class="form-info">
    <div class="logo">
       <img src="<?=base_url('public')?>/images/logo-light.png" alt="LetMalaga" class="img-fluid">
    </div>
    <ul>
      <li>
        <div class="icon"><i class="flaticon-message"></i></div>
        <!-- <div class="text-bold">Correo:</div> -->
        <div class="info">hello@letmalaga.com</div>
      </li>
      
      <li>
        <div class="icon"><i class="flaticon-phone-call"></i></div>
        <!-- <div class="text-bold">Telefono:</div> -->
        <div class="info">+34 663 325 793</div>
      </li>

      <li>
        <div class="icon"><i class="flaticon-phone-call"></i></div>
        <!-- <div class="text-bold">Telefono:</div> -->
        <div class="info">+34 951 493 932</div>
      </li>

      <li>
        <div class="icon"><i class="flaticon-skype-logo"></i></div>
        <!-- <div class="text-bold">Skype:</div> -->
        <div class="info">hello@letmalaga.com</div>
      </li>
      
    </ul>
  </div>

  
  <form action="#" validate="off">
    <input type="text" class="input-form" name="fullname" placeholder="Nombre completo">
    <input type="email" class="input-form" name="email" placeholder="Email">
    <input type="text" class="input-form" name="subject" placeholder="Asunto">
    <textarea name="message" class="input-form" placeholder="Escribe tu mensaje"></textarea>
    <div class="actions">
      <div class="social">
        <div class="icons">
          <a href="#" class="icon facebook-color"><i class="laticon-facebook-logo-button"></i></a>
          <a href="#" class="icon twitter-color"><i class="flaticon-twitter-logo-button"></i></a>
          <a href="#" class="icon pinterest-color"><i class="flaticon-pinterest"></i></a>
          <a href="#" class="icon instagram-color"><i class="flaticon-instagram-logo"></i></a>
          <a href="#" class="icon youtube-color"><i class="flaticon-youtube"></i></a>
        </div>
      </div>
      <input type="submit" class="button-submit" value="enviar">
    </div>
  </form>
</div>