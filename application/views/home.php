<?php $this->load->view('partials/header',[
  'background' => true
]); ?>

<div class="bg-image">
  <?php $this->load->view('partials/head',['background' => true]); ?>

  <section class="content-page">

    <div class="container">
      <div class="row justify-content-center align-items-center">
        <div class="col-12 col-sm-12 col-md-7">
          <form action="<?=base_url('filter')?>" id="principal-form" method="get">
            <input type="hidden" name="in"><input type="hidden" name="out">
            <div class="title white text-center title-lg">Déjanos Buscar tu casa</div>
            <div class="sub-title">
              <ul class="sub-title-options">
                <li>Comprar</li>
                <li>-</li>
                <li><b>Alquilar</b></li>
                <li>-</li>
                <li>Alquilar por meses</li>
              </ul>
            </div>
          
            <div class="form">
              <div class="dates">
                <div class="box box-date first">
                  <div class="icon">
                    <span class="flaticon-calendar"></span>
                  </div>
                  <div class="text in-date">Entrada</div>
                  <div class="arrow">
                    <div class="flaticon-down-arrow"></div>
                  </div> 
                </div>
                
                <div class="box box-date last">
                  <div class="icon">
                    <span class="flaticon-calendar"></span>
                  </div>
                  <div class="text out-date">Salida</div>
                  <div class="arrow">
                    <div class="flaticon-down-arrow"></div>
                  </div> 
                </div>
              </div>

              <a href="<?=base_url('about')?>" class="box box-information">
                Información
              </a>
            </div>

            <button type="submit" class="form-button">Buscar</button>
          </form>
        </div>
      </div>
    </div>

  </section>
  
</div>
<section class="content-page">
  
  <div class="container">
    <div class="properties">
      <div class="row">
        <div class="col-12">
          <div class="section-title">lo mas destacado en LetMalaga</div>
          <div class="section-subtitle">Descubre los mejores lugares y maravillosas experiencias para disfrutar en Málaga</div>
        </div>
        <?php foreach ($properties as $property): ?>
          <?php $this->load->view('properties/property-component',[
            'property' => $property,
            'grid' => 'col-lg-4'
          ]); ?>
        <?php endforeach ?>
      </div>
    </div>

    <div class="services-blog">
      <div class="row">
        <div class="col-lg-8">
          <div class="row">
            <div class="col-12">
              <div class="section-title">Nuestros servicios</div>
              <div class="section-subtitle">Todo lo que Letmalaga puede ofrecerte para hacer que tu experiencia sea un sueño</div>
            </div>
            <?php foreach ($services as $service): ?>
              <?php $this->load->view('services/service-component',[
                'service_blog' => $service,
                'type' => 'services'
              ]); ?>
            <?php endforeach ?>
            
          </div>
        </div>

        <div class="col-lg-4">
          <div class="row">
             <div class="col-12">
              <div class="section-title">Contactanos</div>
              <div class="section-subtitle">Envianos tus dudas acerca de nuetsro servicio</div>
            </div>
            <div class="contact-form">
              <div class="form-info">
                <div class="logo">
                   <img src="<?=base_url('public')?>/images/logo-dark.png" alt="LetMalaga" class="img-fluid">
                </div>
                <ul>
                  <li>
                    <div class="icon"><i class="flaticon-message"></i></div>
                    <!-- <div class="text-bold">Correo:</div> -->
                    <div class="info">hello@letmalaga.com</div>
                  </li>
                  
                  <li>
                    <div class="icon"><i class="flaticon-phone-call"></i></div>
                    <!-- <div class="text-bold">Telefono:</div> -->
                    <div class="info">+34 663 325 793</div>
                  </li>

                  <li>
                    <div class="icon"><i class="flaticon-phone-call"></i></div>
                    <!-- <div class="text-bold">Telefono:</div> -->
                    <div class="info">+34 951 493 932</div>
                  </li>

                  <li>
                    <div class="icon"><i class="flaticon-skype-logo"></i></div>
                    <!-- <div class="text-bold">Skype:</div> -->
                    <div class="info">hello@letmalaga.com</div>
                  </li>
                  
                </ul>
              </div>

              
              <form action="#" validate="off">
                <input type="text" class="input-form" name="fullname" placeholder="Nombre completo">
                <input type="email" class="input-form" name="email" placeholder="Email">
                <input type="text" class="input-form" name="subject" placeholder="Asunto">
                <textarea name="message" class="input-form" placeholder="Escribe tu mensaje"></textarea>
                <div class="actions">
                  <div class="social">
                    <div class="icons">
                      <a href="#" class="icon facebook-color"><i class="laticon-facebook-logo-button"></i></a>
                      <a href="#" class="icon twitter-color"><i class="flaticon-twitter-logo-button"></i></a>
                      <a href="#" class="icon pinterest-color"><i class="flaticon-pinterest"></i></a>
                      <a href="#" class="icon instagram-color"><i class="flaticon-instagram-logo"></i></a>
                      <a href="#" class="icon youtube-color"><i class="flaticon-youtube"></i></a>
                    </div>
                  </div>
                  <input type="submit" class="button-submit" value="enviar">
                </div>
              </form>
            </div>
          </div>
          
        </div>
        
      </div>        
    </div>

    <div class="services-blog">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-12">
              <div class="section-title">Noticias de Letmalaga</div>
              <div class="section-subtitle">Ultimas noticias</div>
            </div>
            <?php foreach ($posts as $post): ?>
              <?php $this->load->view('services/service-component',[
                'service_blog' => $post,
                'type' => 'article'
              ]); ?>
            <?php endforeach ?>
          </div>
        </div>
        
      </div>        
    </div>
  </div>

</section>


<?php $this->load->view('partials/footer'); ?>