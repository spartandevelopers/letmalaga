<script>

let center = [<?=$properties[0]->latitude ?>,<?=$properties[0]->longitude?>]

var map = L.map('map').setView(center, 18);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
  maxZoom: 18,
 
  id: 'mapbox.streets'
}).addTo(map);

let markers = [];

<?php foreach ($properties as $property): ?>
  markers.push(marker = L.marker([<?=$property->latitude ?>,<?=$property->longitude?>]).addTo(map));

  popupContent = `<?=$this->load->view('draw/popup-marker',['property' => $property],true);?>`;
  marker.bindPopup(popupContent);
  

<?php endforeach ?>
  




</script>