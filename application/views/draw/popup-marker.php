<?php
  $index = count($property->images) - 1;
  $folder = (isset($property->images[$index]['principal']) and $property->images[$index]['principal']) ? 'posters/' : $property->images[$index]['size'].'/'; 


?>

<div class="property-map">
  <div class="banner-map" style="background-image: url('<?=base_url('public/images/'.$folder.$property->images[$index]['image'])?>');"></div>
  <div class="info-property-map">
    <div class="title-property-map"><?=$property->name?></div>
      <div class="extra-info-map">
        <div class="address-property-map"><?=$property->address?></div>
        <div class="price">
          <?=$property->minimum_rate ?>€ / Día
        </div>
      </div>
  </div>
</div>

