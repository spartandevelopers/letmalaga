<?php $this->load->view('partials/header'); ?>

<style>
  iframe{
    width: 100%;
    height: 100%;
    min-height: 600px;
    /*max-height: 600px;*/
    overflow:hidden;
  }
</style>

<section id="content-page" class="search-bg">
  <div class="container">
   <div class="services-blog">
      <div class="row">

        <div class="col-12">
          <div class="section-title">Reserva <?=$property->name?></div>
          <div class="section-subtitle"><?=$property->description?></div>
        </div>
        <div class="col-lg-8">
          <iframe src="<?=$iframe?>" id="iframe" frameborder="0"></iframe>
        </div>

        <div class="col-lg-4">
          <div class="row">
            <div class="col-12">
              <?php
                $banner = $this->banner->get(['type' => 'ads'])[0];
              ?>
              <div class="banner-vertical" style="background-image: url(<?=base_url('public/images/banners/'.$banner->file_name)?>)">
                
              </div>
            </div>
          </div>
          
        </div>
        
      </div>        
    </div>
  </div>
</section>



<?php $this->load->view('partials/footer'); ?>