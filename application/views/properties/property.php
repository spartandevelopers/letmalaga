<?php $this->load->view('partials/header'); ?>

<?php $max = $property->maximum_capacity;?>


<section id="content-page">
  <div class="container">
    <div class="row reorder">
      <div class="col-12">
        <div class="breadcrumb-letmalaga">
          <a href="<?=base_url('filter?').http_build_query($this->input->get())?>" class="return-back text-uppercase">Volver al listado</a>
        </div>
      </div>
      <div class="col-lg-9">
        
        <div class="property">
          <div class="banner"></div>
          <div class="info-property-principal">
            <div class="name"><?=$property->name?></div>
            <div class="address-info">
              <i class="flaticon-maps-and-flags icon"></i>
              <div class="address"><?=$property->address?></div>
            </div>
            <div class="characteristics">
              <ul>
                <li><span class="info"><?=$property->surface?>m</span><span class="sub">2</span></li>
                <li><i class="flaticon-avatar icon"></i> <div class="info"><?=$max?></div></li>
                <li><i class="flaticon-bed icon"></i> <div class="info"><?=$property->number_of_rooms?></div></li>
                <li><i class="flaticon-bathtub icon"></i> <div class="info"><?=$property->number_of_bathrooms?></div></li>
              </ul>
            </div>
          </div>
          <div class="info-property">
            <div class="section">
              <div class="section-title text-uppercase">Datos de la propiedad</div>
              <div class="section-content">

                <?php foreach ($property->descriptions as $description): ?>
                  <p><?=$description->text?></p>
                <?php endforeach ?>
                
              </div>
            </div>

            <div class="section">
              <div class="section-title text-uppercase">Caracteristicas</div>
              <div class="section-content characteristics-property">
                <ul>
                  <?php foreach ($property->addons as $add): ?>
                    <li class="col-md-4 col-sm-12">
                      <?php if ($add->icon != null): ?>
                        <i class="<?=$add->icon?> icon"></i>
                      <?php endif ?>
                      <div class="info"><?=$add->name?></div>
                    </li>
                  <?php endforeach ?>
                </ul>
              </div>
            </div>

            <style>
              .hiden{
                display: none;
                
              }
            </style>

            <div class="section">
              <div class="section-title text-uppercase">fotos</div>
              <div class="section-content">
                <div class="gallery">
                  <?php foreach ($property->images as $key => $image): ?>

                    <?php if (!isset($image['principal'])): ?>
                      <?php 
                        $size = explode('x',$image['size'])[0];
                        $image['image'] = str_replace($size,'',$image['image']);
                        $thumb_name = explode('.',$image['image']);
                        $thumb_name[0] .= $size;
                        $thumb_name = implode('.',$thumb_name);

                        $fullname = explode('.',$image['image']);
                        $fullname[0] .= '700';
                        $fullname = implode('.',$fullname);

                      ?>

                      <a href="<?=base_url('public/images/700x525/'.$fullname)?>" class="image <?=($key > 5) ? 'hiden' : '' ?>">
                        <img src="<?=base_url('public/images/'.$image['size'].'/'.$thumb_name)?>" alt="<?=$property->name.'-'.$image['image']?>" class="img-fluid">
                      </a>
                      
                    <?php endif ?>
                    
                  <?php endforeach ?>
                </div>

                <div class="action">
                  <button type="button" class="form-button gallery-button">Ver Más Fotos</button>
                </div>
              </div>
            </div>

            <div class="section">
              <div class="section-title text-uppercase">la zona</div>
              <div class="section-content">
                <div class="map" id="map-property">
                </div>
              </div>
            </div>

            <div class="section">
              <div class="section-title text-uppercase">Propiedades similares</div>
              <div class="section-content">
                <div class="properties">
                  <div class="row">
                    <?php foreach ($alternative_properties as $property): ?>
                      <?php $this->load->view('properties/property-component',[
                        'property' => $property,
                        'grid' => 'col-lg-6 col-sm-12'
                      ]); ?>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <style>
        .input-box{
          border:none;
          padding: 5px;
          width: 100%;
          height: 100%;
        }
      </style>
      <div class="col-lg-3 order-1" style="position: relative;">
        <div class="reserve">
          <form class="content" method="get" action="<?=base_url('reserve')?>">
            <input type="hidden" name="in" value="<?=$this->input->get('in')?>" required>
            <input type="hidden" name="out" value="<?=$this->input->get('out')?>" required>
            <input type="hidden" name="hot" value="<?=$property->id?>" required>
            <div class="reserve-name">
              <?=$property->name?>
            </div>
            <div class="reserve-form">
             <div class="dates new">
                <div class="box box-date new first">
                 <div class="icon">
                   <span class="flaticon-calendar"></span>
                 </div>
                 <div class="text in-date"><?=$this->input->get('in')?></div>
                 <div class="arrow">
                   <div class="flaticon-down-arrow"></div>
                 </div> 
               </div>
               
               <div class="box box-date new last">
                 <div class="icon">
                   <span class="flaticon-calendar"></span>
                 </div>
                 <div class="text out-date"><?=$this->input->get('out')?></div>
                 <div class="arrow">
                   <div class="flaticon-down-arrow"></div>
                 </div> 
               </div>
             </div>
              <div class="box">
                <input type="number" max="<?=$max?>" required name="persons" class="input-box" min="1" placeholder="Maximo : <?=$max?> Personas">
              </div>

              <button type="submit" class="box box-information">
                Reservar
              </button>
            </div>
          </form>
        </div>
        <?php
          $banner = $this->banner->get(['type' => 'ads'])[0];
        ?>
        <div class="banner-vertical" style="background-image: url(<?=base_url('public/images/banners/'.$banner->file_name)?>)">
          
        </div>
      </div>
    </div>
  </div>
</section>

<?php $this->load->view('partials/footer'); ?>

<script>

let center = [<?=$property->latitude ?>,<?=$property->longitude?>]

var map = L.map('map-property').setView(center, 18);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
  maxZoom: 18,
 
  id: 'mapbox.streets'
}).addTo(map);

let markers = [];
markers.push(marker = L.marker([<?=$property->latitude ?>,<?=$property->longitude?>]).addTo(map));

popupContent = `<?=$this->load->view('draw/popup-marker',['property' => $property],true);?>`;
marker.bindPopup(popupContent);




</script>

<script>

  $('.dates.new').flatpickr({
    mode: "range",
    minDate: "today",
    dateFormat: "d-m-Y",
    defaultDate: '<?=$this->input->get('in')?>',
    locale: "es",
    disable: <?=$disables_dates?>,
    onChange: function(selectedDates, dateStr, instance) {
      let date = dateStr.split('a');
      $('.in-date').html(date[0]);
      $('.out-date').html(date[1]);
      $('input[name=in]').val(date[0]);
      $('input[name=out]').val(date[1]);

    },

  }); 


  // let picker2 = $('.box.box-date.new.first').flatpickr({
  //   mode: "range",
  //   minDate: "today",
  //   dateFormat: "d-m-Y",
  //   defaultDate: '<?=$this->input->get('in')?>',
  //   locale: "es",
  //   disable: <?=$disables_dates?>,
  //   onChange: function(selectedDates, dateStr, instance) {
  //     let date = dateStr.split('a');
  //     $('.in-date').html(date[0]);
  //     $('.out-date').html(date[1]);
  //     $('input[name=in]').val(date[0]);
  //     $('input[name=out]').val(date[1]);

  //   },

  // }); 

  // $('.box.box-date.new.last').on('click', function() {
  //   picker2.open()
  // });


</script>