<?php 
  
  if (isset($get)) {

    $url = base_url('property/'.url_title($property->name)).'?'.http_build_query($get);
  }else{
    $url = base_url('property/'.url_title($property->name));
  }

  
?>

<div class="<?=$grid?>">
  <a href="<?=$url?>" class="property">
    <div class="owl-carousel owl-theme carousel">
      <?php foreach ($property->images as $image): ?>
        <?php $folder = (isset($image['principal']) and $image['principal']) ? 'posters/' : $image['size'].'/' ?>
        <div class="item">
          <div class="mini-banner" style="background-image: url(<?=base_url('public/images/'.$folder.$image['image'])?>)"></div>
        </div>
      <?php endforeach ?>
    </div>

    <div class="info-property">
      <div class="property-title"><?=$property->name?></div>
      <div class="property-address">
        <i class="flaticon-maps-and-flags icon"></i>
        <div class="address"><?=$property->address?></div>
      </div>
      <div class="characteristics">
        <ul>
          <li><span class="info"><?=$property->surface?>m</span><span class="sub">2</span></li>
          <li><i class="flaticon-avatar icon"></i> <div class="info"><?=$property->maximum_capacity?></div></li>
          <li><i class="flaticon-bed icon"></i> <div class="info"><?=$property->number_of_rooms?></div></li>
          <li><i class="flaticon-bathtub icon"></i> <div class="info"><?=$property->number_of_bathrooms?></div></li>
        </ul>
        <div class="price">
          <?=$property->minimum_rate?>€ / Dia
        </div>
      </div>
    </div>
  </a>
</div>