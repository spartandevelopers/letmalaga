<?php $this->load->view('partials/header'); ?>


<section class="content-page">

  <div class="billboard">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="billboard-title">Politica de privacidad</div>
        </div>
      </div>
    </div>
    
  </div>
  
  <div class="container">
    

      <div class="row">
        <div class="col-lg-8">
          <div class="panel-text">
            <!-- <div class="about-text">
              
                <p>
                  Mediante este aviso, <b>Proyecto LetMalaga</b> como propietario del dominio www.LetMalaga.com informa a los usuarios de su red de sitios web acerca de su Política de protección de datos de carácter personal para que los usuarios determinen libre y voluntariamente si desean facilitar a <b>Proyecto LetMalaga</b> los datos personales que se les puedan requerir o que se puedan obtener de los usuarios con ocasión de la suscripción o alta en algunos de los servicios ofrecidos por <b>Proyecto LetMalaga</b> por medio de su red de sitios web.
                </p>
                <p>
                  <b>Proyecto LetMalaga</b> se reserva el derecho a modificar la presente política para adaptarla a novedades legislativas o jurisprudenciales así como a prácticas de la industria. En dichos supuestos, <b>Proyecto LetMalaga</b> anunciará en esta página los cambios introducidos con razonable antelación a su puesta en práctica.
                </p>
                <p>
                  Ciertos servicios prestados en la red de sitios web de <b>Proyecto LetMalaga</b> pueden contener condiciones particulares con previsiones específicas en materia de protección de Datos Personales. Los datos personales recogidos serán objeto de tratamiento automatizado e incorporados a los correspondientes ficheros automatizados por <b>Proyecto LetMalaga</b> siendo ésta titular y responsable de su propio fichero.
                </p>
                <p>
                  Con este objeto, <b>Proyecto LetMalaga</b> proporcionará a los usuarios los recursos técnicos adecuados para que, con carácter previo, puedan acceder a este aviso sobre la Política de protección de datos o a cualquier otra información relevante y puedan prestar su consentimiento a fin de que <b>Proyecto LetMalaga</b> proceda al tratamiento automatizado de sus datos personales. Salvo en los campos en que se indique lo contrario, las respuestas a las preguntas sobre datos personales son voluntarias, sin que la falta de contestación implique una merma en la calidad o cantidad de los servicios correspondientes, a menos que se indique otra cosa.
                </p>
                <p>
                  La recogida y tratamiento automatizado de los datos personales tiene como finalidad el mantenimiento de la relación contractual en su caso establecida con <b>Proyecto LetMalaga</b> la gestión, administración, prestación, ampliación y mejora de los servicios en los que el usuario decida suscribirse, darse de alta, o utilizar la adecuación de dichos servicios a las preferencias y gustos de los usuarios, el estudio de la utilización de los servicios por parte de los usuarios, el diseño de nuevos servicios relacionados, el envío de actualizaciones de los servicios, el envío, por medios tradicionales y electrónicos, de información técnica, operativa y comercial acerca de productos y servicios ofrecidos por o a través de <b>Proyecto LetMalaga</b> actualmente y en el futuro.
                </p>
                <p>
                  La finalidad de la recogida y tratamiento automatizado de los datos personales incluye igualmente el envío de formularios de encuestas, que el usuario no queda obligado a contestar. <b>Proyecto LetMalaga</b> ha adoptado los niveles de seguridad de protección de los datos personales legalmente requeridos, y ha instalado todos los medios y medidas técnicas a su alcance para evitar la pérdida, mal uso, alteración, acceso no autorizado y robo de los datos personales facilitados a <b>Proyecto LetMalaga.</b> Ello no obstante, el usuario debe ser consciente de que las medidas de seguridad en Internet no son inexpugnables.
                </p>
                <p>
                  Los usuarios tienen reconocidos y podrán ejercitar los derechos de acceso, cancelación, rectificación y oposición, así como tienen reconocido el derecho a ser informados de las cesiones realizadas contactando con <b>Proyecto LetMalaga</b> a través del correo electrónico LetMalaga@LetMalaga.com <b>Proyecto LetMalaga</b> no cederá los datos personales a terceros, salvo que sea necesario para la prestación del servicio solicitado por el usuario.
                </p>
                <p>
                  Los acuerdos a través de los cuales se hacen dichas cesiones constarán descritos en esta misma Política de protección de datos. <b>Proyecto LetMalaga</b> utiliza cookies y sesiones de usuario cuando se navega por su red de sitios web. Estas cookies se asocian únicamente con un usuario anónimo y su ordenador, y no proporcionan por sí el nombre y apellidos del usuario.
                </p>
                <p>
                  Gracias a las cookies, resulta posible que <b>Proyecto LetMalaga</b> reconozca a los usuarios registrados después de que éstos se hayan registrado por primera vez, sin que tengan que registrarse en cada visita para acceder a las áreas y servicios reservados exclusivamente a ellos. Las cookies utilizadas no pueden leer datos de su disco duro ni leer los archivos cookie creados por otros proveedores.
                </p>
                <p>
                  El Usuario tiene la posibilidad de configurar su navegador para ser avisado en pantalla de la recepción de cookies e incluso impedir la instalación de cookies en su disco duro. Para utilizar la red de sitios web de <b>Proyecto LetMalaga</b> no resulta necesario que el usuario permita la instalación de las cookies enviadas por <b>Proyecto LetMalaga</b> si bien esto puede repercutir en una peor experiencia de navegación por razones técnicas.
                </p>
                <p>
                  Por motivos legales, <b>Proyecto LetMalaga</b> podrá y deberá facilitar cuanta información le sea requerida a las autoridades competentes conforme a las leyes españolas en caso de mediar la pertinente orden judicial, la cual sólo se da cuando un juez tiene firme sospecha de que el usuario ha realizado actividades ilegales.
                </p>
                <p>
                  Bajo este supuesto, y con la intención de colaborar con la justicia, <b>Proyecto LetMalaga</b> puede registrar y posteriormente facilitar a la policía, previa presentación de la orden judicial legalmente necesaria, información relativa a la dirección IP que identifica a la conexión del usuario, así como la hora exacta de la misma, nombres de usuario y contraseñas, entre otros datos.
                </p>
                <p>
                  En cualquier caso, las direcciones IP y horas de conexión se registran sólo en aquellos servicios de los que se sospeche que algún usuario pueda hacer un uso ilegal de los mismos.
                </p>
        
            </div> -->

            <div class="about-text">
              
              <p>&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">“En cumplimiento del artículo 10 de la Ley 34/2002, del 11 de julio, de servicios de la Sociedad de la Información y Comercio Electrónico (LSSICE) se exponen a continuación los datos identificativos de la empresa.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">Ignacio Shaw Morales, Calle Comedias, nº 9 ,planta 6, oficina 2, 29008 Málaga, ESPAÑA (SPAIN) – info@letmalaga.com + 34 692613433-</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">1. Protección de Datos (Política de Privacidad)</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">La política de protección de datos personales, informa a los usuarios del procedimiento llevado a cabo por la organización para recoger los datos personales, permitiendo ver a éstos el uso que se les da, y las opciones que tienen las organizaciones respecto a su recogida.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">Los datos personales recogidos en las webs se deberán tratar de acuerdo con lo dispuesto en la LOPD 15/1999, y deberán ser tratados de manera confidencial.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">Los usuarios pueden ejercitar los derechos de oposición, acceso e información, rectificación, cancelación y revocación de su autorización a la utilización de sus datos personales.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">Esta recogida de datos es general ya que en la mayoría de las páginas web que se visitan en la actualidad se recogen los datos relativos al usuario, ya que los datos personales son elemento imprescindible para la realización de cualquier gestión, o en la relación a distancia con distribuidor, comercio…</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">Por ello se puso en marcha la LOPD, con el fin de regular el tratamiento de estos datos personales. Dicha ley obliga a informar sobre el tratamiento de éstos y los fines por los que la empresa los requiere. Esta información es la que se refleja en los textos donde se recoge la política de privacidad.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">Además la ley también obliga a que se solicite el consentimiento para las cesiones de datos que se realicen entre las empresas. En el entorno web, para que el usuario dé su consentimiento, es necesario que acepte las condiciones que normalmente suelen exponerse en la política de privacidad. Por ello, en todos los formularios que podemos encontrar en la red, debe llevar una pestaña donde podamos marcar que hemos leído y aceptado la política de privacidad.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">2. Política de Privacidad (Protección de Datos)</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">“Por medio de este sitio web se recogen datos de carácter personal necesarios para la gestión y mantenimiento de algunos de nuestros servicios. Dichos datos son incluidos en nuestros ficheros, que se encuentran convenientemente inscritos en el Registro de la Agencia de Protección de Datos.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">Sin perjuicio de las finalidades que en cada caso se indiquen, dicha información será guardada y gestionada con su debida confidencialidad, aplicando las medidas de seguridad informática establecidas en la legislación aplicable para impedir el acceso o uso indebido de sus datos, su manipulación, deterioro o pérdida.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">En cualquier momento puede Ud. ejercer sus derechos de acceso, rectificación o cancelación en relación con dichos datos, dirigiendo su solicitud a la dirección del titular de los ficheros. Puede encontrar formularios para el ejercicio de sus derechos en el sitio web de la Agencia de Protección de Datos”</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">Creación de un Aviso Legal y Política de Privacidad</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <ol>
              <li>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">DATOS IDENTIFICATIVOS: En cumplimiento con el deber de información recogido en artículo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y del Comercio Electrónico, a continuación se reflejan los siguientes datos: la empresa titular de dominio web es Ignacio Shaw Morales (en adelannte letmalaga), con domicilio a estos efectos en Calle Comedias, nº 9, 6ª planta, Oficina 2 número de .I.F.: 44.576.640-H Correo electrónico de contacto: info@letmalaga.com del sitio web.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              </li>
              </ol><ol start="2">
              <li>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">USUARIOS: El acceso y/o uso de este portal de Nombre de la empresa creadora del sitio web atribuye la condición de USUARIO, que acepta, desde dicho acceso y/o uso, las Condiciones Generales de Uso aquí reflejadas. Las citadas Condiciones serán de aplicación independientemente de las Condiciones Generales de Contratación que en su caso resulten de obligado cumplimiento.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              </li>
              </ol><ol start="3">
              <li>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">USO DEL PORTAL: indicar dominio proporciona el acceso a multitud de informaciones,</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              </li>
              <li>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">servicios, programas o datos (en adelante, "los contenidos") en Internet pertenecientes a Nombre de la empresa creadora del sitio web o a sus licenciantes a los que el USUARIO pueda tener acceso. El USUARIO asume la responsabilidad del uso del portal. Dicha responsabilidad se extiende al registro que fuese necesario para acceder a determinados servicios o contenidos. En dicho registro el USUARIO será responsable de aportar información veraz y lícita. Como consecuencia de este registro, al USUARIO se le puede proporcionar una contraseña de la que será responsable, comprometiéndose a hacer un uso diligente y confidencial de la misma. El USUARIO se compromete a hacer un uso adecuado de los contenidos y servicios (como por ejemplo servicios de chat, foros de discusión o grupos de noticias) que Nombre de la empresa creadora del sitio web ofrece a través de su portal y con carácter enunciativo pero no limitativo, a no emplearlos para (i) incurrir en actividades ilícitas, ilegales o contrarias a la buena fe y al orden público; (ii) difundir contenidos o propaganda de carácter racista, xenófobo, pornográfico-ilegal, de apología del terrorismo o atentatorio contra los derechos humanos; (iii) provocar daños en los sistemas físicos y lógicos de Nombre de la empresa creadora del sitio web , de sus proveedores o de terceras personas, introducir o difundir en la red virus informáticos o cualesquiera otros sistemas físicos o lógicos que sean susceptibles de provocar los daños anteriormente mencionados; (iv) intentar acceder y, en su caso, utilizar las cuentas de correo electrónico de otros usuarios y modificar o manipular sus mensajes. Nombre de la empresa creadora del sitio web se reserva el derecho de retirar todos aquellos comentarios y aportaciones que vulneren el respeto a la dignidad de la persona, que sean discriminatorios, xenófobos, racistas, pornográficos, que atenten contra la juventud o la infancia, el orden o la seguridad pública o que, a su juicio, no resultaran adecuados para su publicación. En cualquier caso, Nombre de la empresa creadora del sitio web no será responsable de las opiniones vertidas por los usuarios a través de los foros, chats, u otras herramientas de participación.</p>
              </li>
              </ol>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">4. PROTECCIÓN DE DATOS: Nombre de la empresa creadora del sitio web cumple con las directrices de la Ley Orgánica 15/1999 de 13 de diciembre de Protección de Datos de Carácter Personal, el Real Decreto 1720/2007 de 21 de diciembre por el que se aprueba el Reglamento de desarrollo de la Ley Orgánica y demás normativa vigente en cada momento, y vela por garantizar un correcto uso y tratamiento de los datos personales del usuario. Para ello, junto a cada formulario de recabo de datos de carácter personal, en los servicios que el usuario pueda solicitar a Nombre de la empresa creadora del sitio web , hará saber al usuario de la existencia y aceptación de las condiciones particulares del tratamiento de sus datos en cada caso, informándole de la responsabilidad del fichero creado, la dirección del responsable, la posibilidad de ejercer sus derechos de acceso, rectificación, cancelación u oposición, la finalidad del tratamiento y las comunicaciones de datos a terceros en su caso. Asimismo, Nombre de la empresa creadora del sitio web informa que da cumplimiento a la Ley 34/2002 de 11 de julio, de Servicios de la Sociedad de la Información y el Comercio Electrónico y le solicitará su consentimiento al tratamiento de su correo electrónico con fines comerciales en cada momento.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">5. PROPIEDAD INTELECTUAL E INDUSTRIAL: Nombre de la empresa creadora del sitio web por sí o como cesionaria, es titular de todos los derechos de propiedad intelectual e industrial de su página web, así como de los elementos contenidos en la misma (a título enunciativo, imágenes, sonido, audio, vídeo, software o textos; marcas o logotipos, combinaciones de colores, estructura y diseño, selección de materiales usados, programas de ordenador necesarios para su funcionamiento, acceso y uso, etc.), titularidad de Nombre de la empresa creadora del sitio web o bien de sus licenciantes. Todos los derechos reservados. En virtud de lo dispuesto en los artículos 8 y 32.1, párrafo segundo, de la Ley de Propiedad Intelectual, quedan expresamente prohibidas la reproducción, la distribución y la comunicación pública, incluida su modalidad de puesta a disposición, de la totalidad o parte de los contenidos de esta página web, con fines comerciales, en cualquier soporte y por cualquier medio técnico, sin la autorización de Nombre de la empresa creadora del sitio web . El USUARIO se compromete a respetar los derechos de Propiedad Intelectual e Industrial titularidad de Nombre de la empresa creadora del sitio web. Podrá visualizar los elementos del portal e incluso imprimirlos, copiarlos y almacenarlos en el disco duro de su ordenador o en cualquier otro soporte físico siempre y cuando sea, única y exclusivamente, para su uso personal y privado. El USUARIO deberá abstenerse de suprimir, alterar, eludir o manipular cualquier dispositivo de protección o sistema de seguridad que estuviera instalado en el las páginas de Nombre de la empresa creadora del sitio web.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">6.EXCLUSIÓN DE GARANTÍAS Y RESPONSABILIDAD: Nombre de la empresa creadora del sitio web. no se hace responsable, en ningún caso, de los daños y perjuicios de cualquier naturaleza que pudieran ocasionar, a título enunciativo: errores u omisiones en los contenidos, falta de disponibilidad del portal o la transmisión de virus o programas maliciosos o lesivos en los contenidos, a pesar de haber adoptado todas las medidas tecnológicas necesarias para evitarlo.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">7. MODIFICACIONES: Nombre de la empresa creadora del sitio web se reserva el derecho de efectuar sin previo aviso las modificaciones que considere oportunas en su portal, pudiendo</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">cambiar, suprimir o añadir tanto los contenidos y servicios que se presten a través de la misma como la forma en la que éstos aparezcan presentados o localizados en su portal.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">8. ENLACES: En el caso de que en nombre del dominio se dispusiesen enlaces o hipervínculos hacía otros sitios de Internet, Nombre de la empresa creadora del sitio web no ejercerá ningún tipo de control sobre dichos sitios y contenidos. En ningún caso Nombre de la empresa creadora del sitio web asumirá responsabilidad alguna por los contenidos de algún enlace perteneciente a un sitio web ajeno, ni garantizará la disponibilidad técnica, calidad, fiabilidad, exactitud, amplitud, veracidad, validez y constitucionalidad de cualquier material o información contenida en ninguno de dichos hipervínculos u otros sitios de Internet. Igualmente la inclusión de estas conexiones externas no implicará ningún tipo de asociación, fusión o participación con las entidades conectadas.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">9.DERECHO DE EXCLUSIÓN: Nombre de la empresa creadora del sitio web se reserva el derecho a denegar o retirar el acceso a portal y/o los servicios ofrecidos sin necesidad de preaviso, a instancia propia o de un tercero, a aquellos usuarios que incumplan las presentes Condiciones Generales de Uso.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">10.GENERALIDADES: Nombre de la empresa creadora del sitio web perseguirá el incumplimiento de las presentes condiciones así como cualquier utilización indebida de su portal ejerciendo todas las acciones civiles y penales que le puedan corresponder en derecho.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">11.MODIFICACIÓN DE LAS PRESENTES CONDICIONES Y DURACIÓN: Nombre de la empresa creadora del sitio web podrá modificar en cualquier momento las condiciones aquí determinadas, siendo debidamente publicadas como aquí aparecen. La vigencia de las citadas condiciones irá en función de su exposición y estarán vigentes hasta que sean modificadas por otras debidamente publicadas.</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">&nbsp;</p>
              <p style="margin-bottom: 0cm;" align="JUSTIFY">12. LEGISLACIÓN APLICABLE Y JURISDICCIÓN: La relación entre Nombre de la empresa creadora del sitio web y el USUARIO se regirá por la normativa española vigente y cualquier controversia se someterá a los Juzgados y tribunales de la ciudad de Málaga .</p>
              <p>&nbsp;</p>
                      
                      
    
            </div>

          </div>
        </div>

        <div class="col-lg-4">
          <?php $this->load->view('contact-form'); ?>
        </div>
        
      </div>        
    

  </div>

</section>


<?php $this->load->view('partials/footer'); ?>

