<?php $this->load->view('partials/header'); ?>


<section class="content-page">

  <div class="billboard">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="billboard-title">Politica de cookies</div>
        </div>
      </div>
    </div>
    
  </div>
  
  <div class="container">
    

      <div class="row">
        <div class="col-lg-8">
          <div class="panel-text">
            <div class="about-text">
              <p>
                En la web <b>Proyecto LetMalaga</b> utilizamos cookies para facilitar la relación de los visitantes connuestro contenido y para permitir elaborar estadísticas sobre las visitantes que recibimos.
              </p>
              <p>
                En cumplimiento de la Directiva 2009/136/CE, desarrollada en nuestro ordenamiento por el apartado segundo del artículo 22 de la Ley de Servicios de Sociedad de la Información, siguiendo las directrices de la Agencia Española de Protección de Datos, procedemos a informarle detalladamente del uso que se realiza en nuestra web.
              </p>
              <p>
                Se denominan cookies a unos pequeños archivos que se graban en el navegador utilizado por cada visitante de nuestra web para que el servidor pueda recordar la visita de ese usuario con posterioridad cuando vuelva a acceder a nuestros contenidos. Esta información no revela su identidad, ni dato personal alguno, ni accede al contenido almacenado en su pc, pero sí que permite a nuestro sistema identificarle a usted como un usuario determinado que ya visitó la web con anterioridad, visualizó determinadas páginas, etc. y además permite guardar sus preferencias personales e información técnica como por ejemplo las visitas realizadas o páginas concretas que visite.
              </p>
              <p>
                La finalidad de las cookies es la de facilitar al Usuario un acceso más rápido a los Servicios seleccionados.
              </p>
              <p>
                Si usted no desea que se guarden cookies en su navegador o prefiere recibir una información cada vez que una cookie solicite instalarse, puede configurar sus opciones de navegación para que se haga de esa forma. La mayor parte de los navegadores permiten la gestión de las cookies de 3 formas diferentes:
              </p>
              <p>
                </p><ul style="list-style: none">
                  <li>
                    -Las cookies son siempre rechazadas;
                  </li>
                  <li>
                    - El navegador pregunta si el usuario desea instalar cada cookie;
                  </li>
                  <li>
                    - Las cookies son siempre aceptadas;
                  </li>
                </ul>
              <p></p>
              <p>
                Su navegador también puede incluir la posibilidad de seleccionar con detalle las cookies que desea que se instalen en su ordenador. En concreto, el usuario puede normalmente aceptar alguna de las siguientes opciones:
              </p>
              <p>
                </p><ul style="list-style: none"> 
                  <li>
                    - rechazar las cookies de determinados dominios; 
                  </li>
                  <li>
                    - rechazar las cookies de terceros; 
                  </li>
                  <li>
                    - aceptar cookies como no persistentes (se eliminan cuando el navegador se cierra); 
                  </li>
                  <li>
                    - permitir al servidor crear cookies para un dominio diferente. 
                  </li>
                </ul>
              <p></p>
              <p>
                Para permitir, conocer, bloquear o eliminar las cookies instaladas en su equipo puede hacerlo mediante la configuración de las opciones del navegador instalado en su ordenador.
              </p>
              <p>
                Puede encontrar información sobre cómo configurar los navegadores más usados en las siguientes ubicaciones:
              </p>
              <p>
                </p><ul>
                  <li>
                    <b>Internet Explorer:</b> Herramientas -&gt;; Opciones de Internet -&gt;; Privacidad -&gt;; Configuración. Para más información, puede consultar el soporte de Microsoft o la Ayuda del navegador.
                  </li>
                  <li>
                    <b>Firefox:</b> Herramientas -&gt;; Opciones -&gt;; Privacidad -&gt;; Historial -&gt;; Configuración Personalizada. Para más información, puede consultar el soporte de Mozilla o la Ayuda del navegador.
                  </li>
                  <li>
                    <b>Chrome:</b> Configuración -&gt;; Mostrar opciones avanzadas -&gt;; Privacidad -&gt;; Configuración de contenido. Para más información, puede consultar el soporte de Google o la Ayuda del navegador.
                  </li>
                  <li>
                    <b>Safari:</b> Preferencias -&gt;; Seguridad. Para más información, puede consultar el soporte de Apple o la Ayuda del navegador.
                  </li>
                </ul>
              <p></p>
              <p>
                Respecto de las cookies de terceros, es decir aquellas que son ajenas a nuestro sitio web, no podemos hacernos responsables del contenido y veracidad de las políticas de privacidad que ellos incluyen por lo que la información que le ofrecemos es siempre con referencia a la fuente.
              </p>
              <p>
                A continuación le informamos detalladamente de las cookies que pueden instalarse desde nuestro sitio web. En función de su navegación podrán instalarse todas o sólo algunas de ellas.
              </p>
              <p>
                Se puede modificar esta Política de Cookies en función de exigencias legislativas, reglamentarias, o con la finalidad de adaptar dicha política a las instrucciones dictadas por la Agencia Española de Protección de Datos, por ello se aconseja a los Usuarios que la visiten periódicamente.
              </p>
              <p>
                Cuando se produzcan cambios significativos en esta Política de Cookies, se comunicarán a los Usuarios bien mediante la web o a través de correo electrónico a los Usuarios registrados.
              </p>
        
        
            </div>

          </div>
        </div>

        <div class="col-lg-4">
          <?php $this->load->view('contact-form'); ?>
        </div>
        
      </div>        
    

  </div>

</section>


<?php $this->load->view('partials/footer'); ?>

