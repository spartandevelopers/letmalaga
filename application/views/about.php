<?php $this->load->view('partials/header'); ?>


<section class="content-page">

  <div class="billboard">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="billboard-title">Sobre nosotros</div>
        </div>
      </div>
    </div>
    
  </div>
  
  <div class="container">
    

      <div class="row">
        <div class="col-lg-8">
          <div class="panel-text">
            <div class="about-text">
              <p>Desde hace tiempo vengo pensando que los inmuebles deben de estar preparados para cualquier oportunidad que surja en el mercado, es decir, preparados para una estancia de dos días como de tres meses, unido a &nbsp;una gestión dinámica, es posbile, tenia pensado hasta el nombre "Activo Vivo" pero por ser un nombre muy técnico se ha descartado para este proyecto, pero resumen muy bien la idea, los inmuebles están vivos, siempre rindiendo, son productos perecederos día que no se alquila día que no produce.&nbsp;</p>
              <p>En el año 2010 empeze a gestionar varios edificios de alquiler de larga temporada en el centro de Málaga, pero el mercado empujó abrime camino en alquiler vacacional,</p>
              <p>Hace poco mas de dos años empezamos mi mujer y yo por 3 apartamentos y ya llevamos gestionando 70, el crecimiento ha sido y está siendo espectacular.</p>
              <p>Ahora somos un equipo de 15 personas, y la caracteristicas de nuestro servicio y apartamentos así los definirian:</p>
              <p>Excelente ubicación, notable calidad, &nbsp;singular diseños (muebles , y detalles decorativos hechos a mamo) y un servicio con un grado de resposabilidad y predispuesto ante el gran reto de solucionar las incidencias durante la estancia. Imbatible ralación calidad-precio.</p>
              <p>Nuestra idea es seguir creciendo para alcanzar un volumen que nos permita acceder a recursos humanos y tecnicos de calidad.</p>
            </div>

          </div>
        </div>

        <div class="col-lg-4">
          <?php $this->load->view('contact-form'); ?>
        </div>
        
      </div>        
    

  </div>

</section>


<?php $this->load->view('partials/footer'); ?>

 <script>

    var myIcon = L.icon({
      iconUrl: 'https://image.flaticon.com/icons/png/512/33/33622.png',
      iconSize: [30, 30],
      iconAnchor: [22, 94],
      popupAnchor: [-3, -76],
      shadowUrl: 'my-icon-shadow.png',
      shadowSize: [68, 95],
      shadowAnchor: [22, 94]
    });

    var mymap = L.map('map-horizontal').setView([44.505, -0.09], 13);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 18,
     
      id: 'mapbox.streets'
    }).addTo(mymap);

    let marker = L.marker([44.505, -0.09]).addTo(mymap);
    

  </script>