<?php $this->load->view('partials/header'); ?>

<section id="content-page" class="search-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <form id="params-filter" action="<?=base_url('filter')?>" method="get">
          <input type="hidden" name="in" value="<?=$this->input->get('in')?>">
          <input type="hidden" name="out" value="<?=$this->input->get('out')?>">
          <div class="dates new">
            <div class="box box-date first new">
              <div class="icon">
                <span class="flaticon-calendar"></span>
              </div>
              <div class="text in-date"><?=($this->input->get('in') != null) ? $this->input->get('in') : 'Entrada';?></div>
              <div class="arrow">
                <div class="flaticon-down-arrow"></div>
              </div> 
            </div>
            
            <div class="box box-date last new">
              <div class="icon">
                <span class="flaticon-calendar"></span>
              </div>
              <div class="text out-date"><?=($this->input->get('out') != null) ? $this->input->get('out') : 'Salida'?></div>
              <div class="arrow">
                <div class="flaticon-down-arrow"></div>
              </div> 
            </div>
          </div>

          <button type="submit" class="box box-information">
            Buscar <span class="count-filters">(<?=count($this->input->get('filters'))?>)</span>
          </button>

         <button type="button" class="box box-information show-filters">
          <?php if (count($this->input->get('filters'))): ?>
            Ocultar filtros
          <?php else: ?>
            Mostrar filtros
          <?php endif ?>
            
          </button>

          <div class="filters <?=(!count($this->input->get('filters'))) ? 'hide' : ''?>">
            <?php foreach ($addons as $add): ?>
              <?php 
               $checked = '';
                if (in_array($add->id,$this->input->get('filters'))) {
                  $checked = 'checked';
                }
               ?>
              <div class="mdc-form-field">
                <div class="mdc-checkbox">
                    <input type="checkbox" class="mdc-checkbox__native-control" id="<?=$add->name?>" value="<?=$add->id?>" name="filters[]" <?=$checked?> />
                    <div class="mdc-checkbox__background">
                        <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                            <path class="mdc-checkbox__checkmark-path" fill="none" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                        </svg>
                        <div class="mdc-checkbox__mixedmark"></div>
                    </div>
                </div>
                <label for="<?=$add->name?>"><?=$add->name?></label>
              </div>
            <?php endforeach ?>
            
          </div>
        </form>

        <div class="info-search">
          <div class="text-muted">Hemos encontrado <?=$counter?> propiedades</div>
            <!-- <div class="mdc-select">
              <select class="mdc-select__native-control">
                <option value="" selected></option>
                <option value="grains">
                  Bread, Cereal, Rice, and Pasta
                </option>
                <option value="vegetables">
                  Vegetables
                </option>
                <option value="fruit">
                  Fruit
                </option>
              </select>
              <label class="mdc-floating-label">Ordenar por</label>
              <div class="mdc-line-ripple"></div>
            </div> -->
        </div>

        

        <div class="properties">
          <div class="row">
            <?php $in = $this->input->get('in'); $out = $this->input->get('out') ?>
            <?php foreach ($properties as $key => $property): ?>
              <?php if ($key < 6): ?>
                <?php $this->load->view('properties/property-component',[
                  'property' => $property,
                  'grid' => 'col-lg-6',
                  'get' => [
                    'in' => $in,
                    'out' => $out
                  ]
                ]); ?>
              <?php endif ?>
              
            <?php endforeach ?>
            
          </div>
        </div>

        
        <style>
          #paginate{
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 10px 0;
          }
        </style>
        <div class="row">
          <div class="col-12">
            <nav aria-label="Page navigation example" id="paginate">
            <?php echo $paginate  ?>
            </nav>
          </div>
        </div>
      </div>
     <div class="col-lg-4">
        <div id="map"></div>
      </div>
    </div>
  </div>
</section>



<?php $this->load->view('partials/footer'); ?>
<?php $this->load->view('draw/markers',[
  'properties' => $properties
]); ?>

<script>

  let picker2 = $('.dates.new').flatpickr({
    mode: "range",
    minDate: "today",
    dateFormat: "d-m-Y",
    defaultDate: '<?=$this->input->get('in')?>',
    locale: "es",
    onChange: function(selectedDates, dateStr, instance) {
      let date = dateStr.split('a');
      $('.in-date').html(date[0]);
      $('.out-date').html(date[1]);
      $('input[name=in]').val(date[0]);
      $('input[name=out]').val(date[1]);
    },

  }); 

  // $('.box.box-date.new.last').on('click', function() {
  //   picker2.open()
  // });



</script>