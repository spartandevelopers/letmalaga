<?php $this->load->view('partials/header'); ?>


<section class="content-page">

  <div class="billboard">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="billboard-title">Preguntas frecuentes</div>
        </div>
      </div>
    </div>
    
  </div>
  
  <div class="container">
    

      <div class="row">
        <div class="col-lg-8">
          <div class="faqs">
            <?php foreach ($faqs as $faq): ?>
              <div class="faq">
                <div class="faq-title"><?=$faq->answer?></div>
                <div class="faq-description"><?=$faq->response?></div>
              </div>
            <?php endforeach ?>
          </div>
        </div>

        <div class="col-lg-4">
          <?php $this->load->view('contact-form'); ?>
        </div>
        
      </div>        
    

  </div>

</section>


<?php $this->load->view('partials/footer'); ?>

