<?php $this->load->view('panel/partials/header'); ?>

<style>
  .select-icon{
    cursor: pointer;
  }

  .icon-grids .d-flex .description{
    display: none;
  }

  .icon-grids .icon{
    width: 50px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
  }

  .icon-grids .icon:hover,.icon-grids .icon.selected{
    background-color: rgba(0, 0, 0, 0.2);
    /*color: white;*/
  }
</style>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header header-elements-inline">
        <h5 class="card-title">Gestion de iconos</h5>
      </div>

      <table class="table datatable-basic">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Icono</th>
            <th>Tipo</th>
          </tr>
        </thead>
        <tbody>
          
          <?php foreach ($addons as $add): ?>
            <tr>
              <td><?=$add->name?></td>
              <td>
                <?php if ($add->icon == null): ?>
                  <span class="badge badge-flat border-grey text-grey-600 select-icon" data-id="<?=$add->id?>">
                    Sin icono
                  </span>
                <?php else: ?>
                  <span class="badge badge-flat border-brown text-brown-600 select-icon" data-id="<?=$add->id?>">
                   <i class="<?=$add->icon?>"></i>
                  </span>
                <?php endif ?>
              </td>
              <td><?=$add->type?></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div id="icons-modal" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <form action="panel/icons/set" id="icon-form" method="post" class="axios-client">
      <input type="text" name="icon">
      <input type="text" name="addon">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Elige un modal</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
      
        <div class="modal-body">
          <?php $this->load->view('panel/icons/icons-grid'); ?>
        </div>
      </div>
    </form>
  </div>
</div>



<?php $this->load->view('panel/partials/footer'); ?>