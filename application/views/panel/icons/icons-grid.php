<div class="row icon-grids">
  <div class="col-auto">
    <div class="icon">
      <i class="icon-home"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-home2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-home5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-home7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-home8"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-home9"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-office"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-city"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-newspaper"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-magazine"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-design"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pencil"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pencil3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pencil4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pencil5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pencil6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pencil7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eraser"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eraser2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eraser3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-quill2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-quill4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pen"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pen-plus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pen-minus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pen2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-blog"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pen6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-brush"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spray"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-color-sampler"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-toggle"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bucket"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-gradient"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eyedropper"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eyedropper2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eyedropper3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-droplet"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-droplet2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-color-clear"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paint-format"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stamp"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-image2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-image-compare"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-images2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-image3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-images3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-image4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-image5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-camera"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shutter"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-headphones"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-headset"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-music"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-album"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-tape"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-piano"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-speakers"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-play"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-clapboard-play"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-clapboard"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-media"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-presentation"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-movie"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-film"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-film2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-film3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-film4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-video-camera"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-video-camera2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-video-camera-slash"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-video-camera3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-dice"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chess-king"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chess-queen"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chess"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-megaphone"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-new"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-connection"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-station"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-satellite-dish2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-feed"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mic2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mic-off2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-book"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-book2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-book-play"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-book3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bookmark"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-books"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-archive"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-reading"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-library2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-graduation2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-text"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-profile"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-empty"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-empty2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-files-empty"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-files-empty2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-plus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-plus2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-minus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-minus2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-download"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-download2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-upload"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-upload2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-check"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-check2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-eye"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-eye2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-text2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-text3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-picture"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-picture2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-music"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-music2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-play"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-play2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-video"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-video2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-copy"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-copy2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-zip"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-zip2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-xml"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-xml2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-css"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-css2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-presentation"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-presentation2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-stats"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-stats2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-locked"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-locked2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-spreadsheet"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-spreadsheet2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-copy3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-copy4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paste"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paste2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paste3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paste4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-search"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-download"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-upload"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-plus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-plus2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-minus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-minus2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-check"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-heart"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-remove"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-open"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-plus3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-minus3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-plus4"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-minus4"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-download2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-upload2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-download3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-upload3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-open2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-folder-open3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-certificate"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cc"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-price-tag"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-price-tag2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-price-tags"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-price-tag3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-price-tags2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-barcode2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-qrcode"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ticket"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-theater"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-store"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-store2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cart"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cart2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cart4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cart5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cart-add"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cart-add2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cart-remove"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-basket"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bag"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-percent"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-coins"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-coin-dollar"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-coin-euro"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-coin-pound"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-coin-yen"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-piggy-bank"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-wallet"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cash"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cash2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cash3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cash4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-credit-card"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-credit-card2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-calculator4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-calculator2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-calculator3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chip"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lifebuoy"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone-slash"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone-wave"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone-plus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone-minus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone-plus2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone-minus2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone-incoming"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone-outgoing"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-phone-hang-up"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-address-book"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-address-book2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-address-book3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-notebook"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-envelop"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-envelop2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-envelop3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-envelop4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-envelop5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mailbox"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pushpin"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-location3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-location4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-compass4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-map"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-map4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-map5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-direction"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-reset"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-history"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-watch"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-watch2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-alarm"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-alarm-add"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-alarm-check"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-alarm-cancel"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bell2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bell3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bell-plus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bell-minus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bell-check"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bell-cross"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-calendar"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-calendar2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-calendar3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-calendar52"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-printer"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-printer2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-printer4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shredder"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mouse"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mouse-left"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mouse-right"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-keyboard"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-typewriter"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-display"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-display4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-laptop"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mobile"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mobile2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-tablet"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mobile3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-tv"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-radio"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cabinet"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-drawer"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-drawer2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-drawer-out"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-drawer-in"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-drawer3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-box"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-box-add"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-box-remove"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-download"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-upload"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-floppy-disk"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-floppy-disks"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-usb-stick"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-drive"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-server"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-menu"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-add"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-remove"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-insert"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-export"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-upload"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-refresh"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-diff"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-edit2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-check"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-arrow"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-database-time2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-undo"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-redo"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rotate-ccw"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rotate-cw"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rotate-ccw2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rotate-cw2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rotate-ccw3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rotate-cw3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flip-vertical2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flip-horizontal2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flip-vertical3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flip-vertical4"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-angle"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shear"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-align-left"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-align-center-horizontal"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-align-right"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-align-top"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-align-center-vertical"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-align-bottom"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-undo2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-redo2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-forward"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-reply"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-reply-all"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubbles"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubbles2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubbles3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubbles4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble-notification"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubbles5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubbles6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubbles7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubbles8"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble8"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble-dots3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble-lines3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble9"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble-dots4"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubble-lines4"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubbles9"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bubbles10"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-user"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-users"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-user-plus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-user-minus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-user-cancel"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-user-block"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-user-lock"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-user-check"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-users2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-users4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-user-tie"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-collaboration"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-vcard"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-hat"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bowtie"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-quotes-left"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-quotes-right"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-quotes-left2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-quotes-right2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-hour-glass"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-hour-glass2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-hour-glass3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spinner"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spinner2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spinner3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spinner4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spinner6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spinner9"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spinner10"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spinner11"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-microscope"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-enlarge"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shrink"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-enlarge3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shrink3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-enlarge5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shrink5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-enlarge6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shrink6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-enlarge7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shrink7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-key"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lock"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lock2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lock4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-unlocked"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lock5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-unlocked2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-safe"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-wrench"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-wrench2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-wrench3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-equalizer"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-equalizer2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-equalizer3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-equalizer4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cog"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cogs"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cog2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cog3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cog4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cog52"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cog6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cog7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-hammer"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-hammer-wrench"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-magic-wand"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-magic-wand2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pulse2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-aid-kit"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bug2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-construction"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-traffic-cone"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-traffic-lights"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pie-chart"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pie-chart2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pie-chart3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pie-chart4"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pie-chart5"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pie-chart6"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pie-chart7"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stats-dots"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stats-bars"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pie-chart8"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stats-bars2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stats-bars3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stats-bars4"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chart"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stats-growth"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stats-decline"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stats-growth2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stats-decline2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stairs-up"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stairs-down"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stairs"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ladder"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rating"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rating2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rating3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-podium"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stars"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-medal-star"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-medal"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-medal2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-medal-first"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-medal-second"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-medal-third"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-crown"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-trophy2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-trophy3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-diamond"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-trophy4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-gift"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pipe"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mustache"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cup2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-coffee"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paw"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-footprint"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rocket"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-meter2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-meter-slow"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-meter-fast"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-hammer2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-balance"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-fire"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-fire2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lab"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-atom"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-atom2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bin"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bin2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-briefcase"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-briefcase3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-airplane2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-airplane3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-airplane4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paperplane"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-car"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-steering-wheel"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-car2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-gas"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bus"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-truck"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bike"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-road"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-train"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-train2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ship"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-boat"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chopper"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cube"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cube2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cube3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cube4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pyramid"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pyramid2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-package"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-puzzle"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-puzzle2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-puzzle3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-puzzle4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-glasses-3d2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-brain"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-accessibility"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-accessibility2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-strategy"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-target"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-target2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shield-check"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shield-notice"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shield2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-racing"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-finish"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-power2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-power3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-switch"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-switch22"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-power-cord"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-clipboard"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-clipboard2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-clipboard3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-clipboard4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-clipboard5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-clipboard6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-playlist"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-playlist-add"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-list-numbered"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-list"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-list2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-more"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-more2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-grid"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-grid2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-grid3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-grid4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-grid52"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-grid6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-grid7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-tree5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-tree6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-tree7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lan"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lan2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lan3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circle-small"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu62"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu8"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu9"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu10"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cloud"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cloud-download"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cloud-upload"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cloud-check"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cloud2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cloud-download2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cloud-upload2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cloud-check2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-import"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-download4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-upload4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-download7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-upload7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-download10"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-upload10"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sphere"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sphere3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-earth"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-link"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-unlink"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-link2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-unlink2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-anchor"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flag3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flag4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flag7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flag8"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-attachment"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-attachment2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eye"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eye-plus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eye-minus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eye-blocked"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eye2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eye-blocked2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eye4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bookmark2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bookmark3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bookmarks"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bookmark4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spotlight2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-starburst"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-snowflake"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-weather-windy"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-fan"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-umbrella"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sun3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-contrast"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bed2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-furniture"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chair"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-star-empty3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-star-half"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-star-full2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-heart5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-heart6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-heart-broken2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-thumbs-up2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-thumbs-down2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-thumbs-up3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-thumbs-down3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-height"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-man"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-woman"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-man-woman"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-yin-yang"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cursor"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cursor2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lasso2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-select2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-point-up"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-point-right"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-point-down"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-point-left"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pointer"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-reminder"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-drag-left-right"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-drag-left"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-drag-right"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-touch"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-multitouch"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-touch-zoom"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-touch-pinch"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-hand"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-grab"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-empty"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-plus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-minus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-star"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-picture"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-down"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-up"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-cancel"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-check"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-text"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-music"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stack-play"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-move"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-dots"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-warning"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-warning22"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-notification2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-question3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-question4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-plus3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-minus3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-plus-circle2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-minus-circle2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cancel-circle2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-blocked"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cancel-square"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cancel-square2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spam"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cross2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cross3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkmark"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkmark3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkmark2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkmark4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spell-check"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-spell-check2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-enter"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-exit"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-enter2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-exit2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-enter3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-exit3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-wall"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-fence"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-play3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pause"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stop"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-previous"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-next"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-backward"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-forward2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-play4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pause2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stop2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-backward2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-forward3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-first"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-last"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-previous2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-next2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eject"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-volume-high"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-volume-medium"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-volume-low"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-volume-mute"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-speaker-left"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-speaker-right"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-volume-mute2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-volume-increase"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-volume-decrease"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-volume-mute5"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-loop"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-loop3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-infinite-square"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-infinite"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-loop4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shuffle"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-wave"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-wave2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-split"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-merge"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up5"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-right5"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down5"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-left5"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up-left2"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up7"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up-right2"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-right7"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down-right2"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down7"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down-left2"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-left7"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up-left3"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up8"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up-right3"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-right8"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down-right3"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down8"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down-left3"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-left8"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circle-up2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circle-right2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circle-down2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circle-left2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-resize7"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-resize8"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-square-up-left"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-square-up"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-square-up-right"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-square-right"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-square-down-right"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-square-down"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-square-down-left"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-square-left"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up15"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-right15"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down15"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-left15"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up16"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-right16"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down16"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-left16"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu-open"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu-open2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu-close"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu-close2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-enter5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-esc"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-enter6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-backspace"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-backspace2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-tab"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-transmission"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sort"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-move-up2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-move-down2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sort-alpha-asc"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sort-alpha-desc"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sort-numeric-asc"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sort-numberic-desc"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sort-amount-asc"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sort-amount-desc"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sort-time-asc"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sort-time-desc"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-battery-6"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-battery-0"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-battery-charging"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-command"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-shift"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ctrl"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-opt"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkbox-checked"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkbox-unchecked"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkbox-partial"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-square"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-triangle"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-triangle2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-diamond3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-diamond4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkbox-checked2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkbox-unchecked2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkbox-partial2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-radio-checked"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-radio-checked2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-radio-unchecked"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkmark-circle"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circle"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circle2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circles"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circles2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-crop"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-crop2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-make-group"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ungroup"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-vector"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-vector2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rulers"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pencil-ruler"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-scissors"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-filter3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-filter4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-font"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ampersand2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ligature"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-font-size"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-typography"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-text-height"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-text-width"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-height2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-width"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-strikethrough2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-font-size2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bold2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-underline2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-italic2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-strikethrough3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-omega"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sigma"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-nbsp"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-page-break"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-page-break2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-superscript"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-subscript"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-superscript2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-subscript2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-text-color"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-highlight"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pagebreak"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-clear-formatting"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-table"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-table2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-insert-template"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pilcrow"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ltr"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rtl"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ltr2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-rtl2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-section"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paragraph-left2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paragraph-center2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paragraph-right2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paragraph-justify2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-indent-increase"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-indent-decrease"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paragraph-left3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paragraph-center3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paragraph-right3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paragraph-justify3"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-indent-increase2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-indent-decrease2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-share"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-share2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-new-tab"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-new-tab2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-popout"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-embed"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-embed2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-markup"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-regexp"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-regexp2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-code"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circle-css"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-circle-code"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-terminal"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-unicode"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-seven-segment-0"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-seven-segment-1"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-seven-segment-2"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-seven-segment-3"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-seven-segment-4"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-seven-segment-5"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-seven-segment-6"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-seven-segment-7"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-seven-segment-8"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-seven-segment-9"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-share3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-share4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-google"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-google-plus"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-google-plus2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-google-drive"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-facebook"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-facebook2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-instagram"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-twitter"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-twitter2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-feed2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-feed3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-youtube"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-youtube2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-youtube3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-vimeo"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-vimeo2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lanyrd"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flickr"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flickr2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flickr3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-picassa"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-picassa2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-dribbble"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-dribbble2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-dribbble3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-forrst"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-forrst2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-deviantart"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-deviantart2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-steam"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-steam2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-dropbox"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-onedrive"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-github"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-github4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-github5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-wordpress"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-wordpress2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-joomla"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-blogger"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-blogger2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-tumblr"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-tumblr2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-yahoo"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-tux"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-apple2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-finder"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-android"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-windows"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-windows8"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-soundcloud"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-soundcloud2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-skype"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-reddit"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-linkedin"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-linkedin2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lastfm"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-lastfm2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-delicious"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stumbleupon"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stumbleupon2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-stackoverflow"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pinterest2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-xing"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-flattr"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-foursquare"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paypal"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-paypal2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-yelp"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-pdf"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-openoffice"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-word"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-excel"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-libreoffice"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-html5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-html52"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-css3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-git"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-svg"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-codepen"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chrome"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-firefox"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-IE"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-opera"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-safari"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-check2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-home4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-people"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkmark-circle2"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up-left32"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up52"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up-right32"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-right6"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down-right32"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down52"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down-left32"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-left52"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-calendar5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-move-alt1"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-reload-alt"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-move-vertical"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-move-horizontal"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-hash"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-bars-alt"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-eye8"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-search4"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-zoomin3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-zoomout3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-add"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-subtract"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-exclamation"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-question6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-close2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-task"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-inbox"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-inbox-alt"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-envelope"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-compose"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-newspaper2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-calendar22"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-hyperlink"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-trash"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-trash-alt"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-grid5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-grid-alt"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-menu6"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-list3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-gallery"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-calculator"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-windows2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-browser"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-portfolio"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-comments"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-screen3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-iphone"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ipad"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-googleplus5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pin"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pin-alt"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cog5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-graduation"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-air"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-droplets"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-statistics"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-pie5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-cross"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-minus2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-plus2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-info3"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-info22"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-question7"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-help"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-warning2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-add-to-list"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-left12"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down12"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up12"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-right13"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-left22"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down22"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up22"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-right22"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-left32"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down32"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up32"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-right32"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-switch2"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-checkmark5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-ampersand"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-alert"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-alignment-align"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-alignment-aligned-to"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-alignment-unalign"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-down132"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-up13"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-left13"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-right14"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-small-down"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-small-left"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-small-right"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-arrow-small-up"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-check"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chevron-down"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chevron-left"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chevron-right"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-chevron-up"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-clippy"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-comment"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-comment-discussion"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-dash"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-diff"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-diff-added"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-diff-ignored"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-diff-modified"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-diff-removed"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-diff-renamed"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-file-media"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-fold"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-gear"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-git-branch"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-git-commit"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-git-compare"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-git-merge"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-git-pull-request"></i>
      >
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-graph"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-law"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-list-ordered"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-list-unordered"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mail5"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mail-read"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mention"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-mirror"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-move-down"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-move-left"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-move-right"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-move-up"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-person"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-plus22"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-primitive-dot"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-primitive-square"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-repo-forked"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-screen-full"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-screen-normal"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-sync"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-three-bars"></i>
      
    </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-unfold"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-versions"></i>
      </div>
  </div>
  <div class="col-auto">
    <div class="icon">
      <i class="icon-x"></i>
      </div>
  </div>
</div>