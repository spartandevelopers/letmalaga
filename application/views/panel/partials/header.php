<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="uri" data-value="<?=base_url()?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/assets/css/layout.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/assets/css/components.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/assets/css/colors.min.css" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  

</head>

<body>

  <!-- Main navbar -->
  <div class="navbar navbar-expand-md navbar-dark bg-brown-300 navbar-static">
    <div class="navbar-brand">
      <a href="index.html" class="d-inline-block">
        <img src="<?=base_url('template')?>/global_assets/images/logo_light.png" alt="">
      </a>
    </div>

    <div class="d-md-none">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
        <i class="icon-tree5"></i>
      </button>
      <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
        <i class="icon-paragraph-justify3"></i>
      </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
            <i class="icon-paragraph-justify3"></i>
          </a>
        </li>
      </ul>

      <span class="navbar-text ml-md-3">
        <span class="badge badge-mark border-orange-300 mr-2"></span>
        Bienvenido, <?= $this->session->email ?>
      </span>

      <ul class="navbar-nav ml-md-auto">

        <li class="nav-item">
          <a href="<?=base_url('panel/logout')?>" class="navbar-nav-link">
            <i class="icon-switch2"></i>
            <span class="d-md-none ml-2">Logout</span>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <!-- /main navbar -->


  <!-- Page content -->
  <div class="page-content">

    <!-- Main sidebar -->
    <?php $this->load->view('panel/partials/sidebar'); ?>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

      <!-- Page header -->
      <!-- <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
          <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Home</span> - Dashboard</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
          </div>

          <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
              <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default">
                <i class="icon-bars-alt text-brown-600"></i>
                <span>Statistics</span>
              </a>
              <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default">
                <i class="icon-calculator text-brown-600"></i>
                <span>Invoices</span>
              </a>
              <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default">
                <i class="icon-calendar5 text-brown-600"></i>
                <span>Schedule</span>
              </a>
            </div>
          </div>
        </div>
      </div> -->
      <!-- /page header -->


      <!-- Content area -->
      <div class="content">