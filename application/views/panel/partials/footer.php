      </div>
      <!-- /content area -->


      <!-- Footer -->
      <div class="navbar navbar-expand-lg navbar-light">
        <div class="text-center d-lg-none w-100">
          <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
          </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-footer">
          <span class="navbar-text">
            &copy; 2018. <a href="#">LetMalaga</a> by <a href="#" target="_blank">LetMalaga</a>
          </span>

          <!-- <ul class="navbar-nav ml-lg-auto">
            <li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
            <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li>
            <li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
          </ul> -->
        </div>
      </div>
      <!-- /footer -->

    </div>
    <!-- /main content -->

  </div>
  <!-- /page content -->

  <!-- Core JS files -->
  <script src="<?=base_url('template')?>/global_assets/js/main/jquery.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/main/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/loaders/blockui.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/ui/ripple.min.js"></script>
  <!-- /core JS files -->

  <script src="<?=base_url('template')?>/global_assets/js/plugins/loaders/progressbar.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/notifications/jgrowl.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/notifications/noty.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/ui/fab.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/ui/sticky.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/ui/prism.min.js"></script>
  <script src="<?=base_url('node_modules')?>/axios/dist/axios.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/uploaders/dropzone.min.js"></script>

  
  <script src="<?=base_url('template')?>/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/editors/summernote/summernote.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/forms/selects/select2.min.js"></script>


 
  <script src="<?=base_url('public')?>/js/app.js"></script>
  <script src="<?=base_url('public')?>/js/axiosClient.js"></script>

  <script src="<?=base_url('template')?>/assets/js/app.js"></script>

</body>
</html>