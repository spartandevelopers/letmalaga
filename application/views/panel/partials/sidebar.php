<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">

  <!-- Sidebar mobile toggler -->
  <div class="sidebar-mobile-toggler text-center">
    <a href="#" class="sidebar-mobile-main-toggle">
      <i class="icon-arrow-left8"></i>
    </a>
    <span class="font-weight-semibold">Navegación</span>
    <a href="#" class="sidebar-mobile-expand">
      <i class="icon-screen-full"></i>
      <i class="icon-screen-normal"></i>
    </a>
  </div>
  <!-- /sidebar mobile toggler -->


  <!-- Sidebar content -->
  <div class="sidebar-content">

    <!-- Main navigation -->
    <div class="card card-sidebar-mobile">
      <ul class="nav nav-sidebar" data-nav-type="accordion">
        <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
        <li class="nav-item">
          <a href="<?=base_url('panel')?>" class="nav-link active">
            <i class="icon-home4"></i>
            <span>
              Dashboard
            </span>
          </a>
        </li>

        <li class="nav-item">
          <a href="<?=base_url('panel/services')?>" class="nav-link">
            <i class="icon-newspaper"></i>
            <span>
              Servicios / Blog
            </span>
          </a>
        </li>

        <li class="nav-item">
          <a href="<?=base_url('panel/faqs')?>" class="nav-link">
            <i class="icon-newspaper"></i>
            <span>
              Preguntas frecuentes
            </span>
          </a>
        </li>

        <li class="nav-item">
          <a href="<?=base_url('panel/icons')?>" class="nav-link">
            <i class="icon-thumbs-up2"></i>
            <span>
              Gestion de iconos
            </span>
          </a>
        </li>
        

        <li class="nav-item nav-item-submenu">
          <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Gestion de banners</span></a>

          <ul class="nav nav-group-sub" data-submenu-title="Layouts">
            <li class="nav-item"><a href="<?=base_url('panel/banners/index/home')?>" class="nav-link active">Banners del home</a></li>
            <li class="nav-item"><a href="<?=base_url('panel/banners/index/ads')?>" class="nav-link active">Banners publicitarios derecho</a></li>
            <li class="nav-item"><a href="http://demo.interface.club/limitless/demo/bs4/Template/layout_6/LTR/material/full/index.html" class="nav-link disabled">Layout 6 <span class="badge bg-transparent align-self-center ml-auto">Coming soon</span></a></li>
          </ul>
        </li>

        <li class="nav-item">
          <a href="<?=base_url()?>" class="nav-link">
            <i class="icon-arrow-left5 "></i>
            <span>
              Volver a la web
            </span>
          </a>
        </li>
      </ul>
      
    </div>
    <!-- /main navigation -->

  </div>
  <!-- /sidebar content -->
  
</div>