<?php $this->load->view('panel/partials/header'); ?>

<style>
  .dz-preview.dz-image-preview{
    max-width: 150px;
  }
</style>

<div class="card">
  <div class="card-header header-elements-inline">
    <h5 class="card-title">Banners del <?=$type?></h5>
  </div>
  

  <div class="card-body">
    <p class="font-weight-semibold">Sube multiples imagenes:</p>
    <form action="<?=base_url('panel/banners/store')?>" class="dropzone" id="dropzone_multiple">
      <input type="hidden" name="type" id="type-banner" value="<?=$type?>">
    </form>
  </div>
</div>

<?php $this->load->view('panel/partials/footer'); ?>