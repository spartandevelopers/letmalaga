<?php $this->load->view('panel/partials/header'); ?>

<script>

  var rand = function() {
    return Math.random().toString(36).substr(2);
  };

  var token = function() {
      return rand() + rand();
  };
  
  localStorage.setItem('token-image', token())

</script>

<div class="row justify-content-center">
  <div class="col-lg-12">
    <form action="panel/blog/post/store" method="post" class="axios-client summernote-form">
      <input type="hidden" name="token">
      <div class="card border-top-3 border-top-brown rounded-top-0">
        <div class="card-header">
          <h6 class="card-title">Crea <?=($this->input->get('type') == 'post') ? 'una entrada' : 'un servicio'?></h6>
        </div>
        
        <div class="card-body">

          <div class="row">
            <?php if ($this->input->get('type') == 'post'): ?>
              <div class="col-lg-7">
                <div class="form-group form-group-feedback form-group-feedback-left">
                  <input type="text" class="form-control" placeholder="Titulo del servicio" name="title">
                  <div class="form-control-feedback">
                    <i class="icon-newspaper text-muted"></i>
                  </div>
                  <span class="form-text text-danger validation" id="validate-title"></span>
                </div>
              </div>
              <div class="col-lg-5">
                <div class="form-group form-group-feedback form-group-feedback-left">
                  <select class="form-control form-control-uniform" data-fouc name="category">
                    <option value="0" selected>Sin categoria</option>
                    <?php foreach ($categories as $category): ?>
                      <option value="<?=$category->id?>"><?=$category->title?></option>
                    <?php endforeach ?>
                  </select>
                  <span class="form-text text-danger validation" id="validate-category"></span>
                </div>
                
              </div>
            <?php else: ?>
              <div class="col-12">
                <div class="form-group form-group-feedback form-group-feedback-left">
                  <input type="text" class="form-control" placeholder="Titulo del servicio" name="title">
                  <div class="form-control-feedback">
                    <i class="icon-newspaper text-muted"></i>
                  </div>
                  <span class="form-text text-danger validation" id="validate-title"></span>
                </div>
              </div>
            <?php endif ?>
            
            <div class="col-lg-8">
              <div class="summernote" id="summernote-area"></div>
            </div>
            <div class="col-lg-4">
              <div action="<?=base_url('panel/services/banner/store')?>" class="dropzone blog" id="dropzone_multiple">
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="text-right">
              <button type="submit" class="btn bg-brown-400">Guardar <i class="icon-paperplane ml-2"></i></button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>


<?php $this->load->view('panel/partials/footer'); ?>

