<?php $this->load->view('panel/partials/header'); ?>

<script>

  var rand = function() {
    return Math.random().toString(36).substr(2);
  };

  var token = function() {
      return rand() + rand();
  };
  
  localStorage.setItem('token-image', '<?=$service->token?>')
  localStorage.setItem('lenght-image', '<?=count($service->image)?>')

</script>

<div class="row justify-content-center">
  <div class="col-lg-12">
    <form action="panel/blog/post/update" method="post" class="axios-client summernote-form">
      <input type="hidden" name="token" value="<?=$service->token?>">
      <input type="hidden" name="id" value="<?=$service->id?>">
      <div class="card border-top-3 border-top-brown rounded-top-0">
        <div class="card-header">
          <h6 class="card-title">Edita Crea <?=($this->input->get('type') == 'post') ? 'una entrada' : 'un servicio'?></h6>
        </div>
        
        <div class="card-body">

          <div class="row">
            <?php if ($this->input->get('type') == 'post'): ?>
              <div class="col-lg-7">
                <div class="form-group form-group-feedback form-group-feedback-left">
                  <input type="text" class="form-control" placeholder="Titulo del post" name="title" value="<?=$service->title?>">
                  <div class="form-control-feedback">
                    <i class="icon-newspaper text-muted"></i>
                  </div>
                  <span class="form-text text-danger validation" id="validate-title"></span>
                </div>
              </div>
              <div class="col-lg-5">
                <div class="form-group form-group-feedback form-group-feedback-left">
                  <select class="form-control"  name="category">
                    <option value="0">Sin categoria</option>
                    <?php foreach ($categories as $category): ?>
                      <?php if ($category->id == $service->category->id): ?>
                        <option value="<?=$category->id?>" selected="selected"><?=$category->title?></option>
                      <?php else: ?>
                        <option value="<?=$category->id?>"><?=$category->title?></option>
                      <?php endif ?>
                      
                    <?php endforeach ?>
                  </select>
                  <span class="form-text text-danger validation" id="validate-category"></span>
                </div>
                
              </div>
            <?php else: ?>
              <div class="col-12">
                <div class="form-group form-group-feedback form-group-feedback-left">
                  <input type="text" class="form-control" placeholder="Titulo del servicio" name="title" value="<?=$service->title?>">
                  <div class="form-control-feedback">
                    <i class="icon-newspaper text-muted"></i>
                  </div>
                  <span class="form-text text-danger validation" id="validate-title"></span>
                </div>
              </div>
            <?php endif ?>
            
            <div class="col-lg-8">
              <div class="summernote" id="summernote-area"><?=$service->description?></div>
            </div>
            <div class="col-lg-4">
              <div action="<?=base_url('panel/services/banner/store')?>" class="dropzone service" id="dropzone_multiple">
                <!-- <input type="hidden" name="type" id="type-banner" value="service"> -->
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="text-right">
              <button type="submit" class="btn bg-brown-400">Guardar <i class="icon-paperplane ml-2"></i></button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>


<?php $this->load->view('panel/partials/footer'); ?>

