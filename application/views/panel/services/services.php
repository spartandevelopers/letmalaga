<?php $this->load->view('panel/partials/header'); ?>
<div class="row">
  <div class="col-lg-5">
    <div class="card">
      <div class="card-header header-elements-inline">
        <h5 class="card-title">Servicios</h5>
        <div class="header-elements">
          <a href="<?=base_url('panel/services/create')?>" class="btn bg-brown-300"> <i class="icon-plus3 mr-2"></i>Crear un nuevo servicio</a>
        </div>
      </div>

      <table class="table datatable-basic">
        <thead>
          <tr>
            <th>Titulo</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          
          <?php foreach ($services as $service): ?>
            <tr>
              <td><a href="<?=base_url('panel/services/edit/'.$service->id)?>"><?=$service->title?></a></td>
              <td class="text-center">
                <div class="list-icons">
                  <div class="dropdown">
                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                      <i class="icon-menu9"></i>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                      <a href="<?=base_url('panel/services/edit/'.$service->id)?>" class="dropdown-item"><i class="icon-pencil"></i> Editar</a>
                      <a href="<?=base_url('panel/services/delete/'.$service->id)?>" class="dropdown-item"><i class="icon-trash"></i> Eliminar</a>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-lg-7">
    <div class="card">
      <div class="card-header header-elements-inline">
        <h5 class="card-title">Entradas del blog</h5>
        <div class="header-elements">
          <a href="<?=base_url('panel/blog/post/create?type=post')?>" class="btn bg-brown-300"> <i class="icon-plus3 mr-2"></i>Crear una nueva entrada</a>
        </div>
      </div>

      <table class="table datatable-basic">
        <thead>
          <tr>
            <th>Titulo</th>
            <th>Categoria</th>
            <th>Fecha de publicación</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          
          <?php foreach ($posts as $post): ?>
            <tr>
              <td><a href="<?=base_url('panel/posts/edit/'.$post->id.'?type=post')?>"><?=$post->title?></a></td>
              <td><a href="<?=base_url('panel/category/edit/'.$post->category->id)?>"><?=$post->category->title?></a></td>
              <td><?=$post->date?></td>
              <td class="text-center">
                <div class="list-icons">
                  <div class="dropdown">
                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                      <i class="icon-menu9"></i>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                      <a href="<?=base_url('panel/posts/edit/'.$post->id.'?type=post')?>" class="dropdown-item"><i class="icon-pencil"></i> Editar</a>
                      <a href="<?=base_url('panel/services/delete/'.$post->id.'?type=post')?>" class="dropdown-item"><i class="icon-trash"></i> Eliminar</a>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
</div>



<?php $this->load->view('panel/partials/footer'); ?>