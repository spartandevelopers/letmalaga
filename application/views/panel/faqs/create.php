<?php $this->load->view('panel/partials/header'); ?>



<div class="row justify-content-center">
  <div class="col-lg-7">
    <form action="panel/faqs/store" method="post" class="axios-client summernote-form">
      <input type="hidden" name="token">
      <div class="card border-top-3 border-top-brown rounded-top-0">
        <div class="card-header">
          <h6 class="card-title">Crea Una pregunta frecuente</h6>
        </div>
        
        <div class="card-body">

          <div class="row">
            <div class="col-12">
              <div class="form-group form-group-feedback form-group-feedback-left">
                <input type="text" class="form-control" placeholder="Titulo de la pregunta" name="answer">
                <div class="form-control-feedback">
                  <i class="icon-newspaper text-muted"></i>
                </div>
                <span class="form-text text-danger validation" id="validate-answer"></span>
              </div>
            </div>
            
            <div class="col-lg-12">
              <div class="summernote" id="summernote-area"></div>
            </div>
          </div>
          <div class="col-12">
            <div class="text-right">
              <button type="submit" class="btn bg-brown-400">Guardar <i class="icon-paperplane ml-2"></i></button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>


<?php $this->load->view('panel/partials/footer'); ?>

