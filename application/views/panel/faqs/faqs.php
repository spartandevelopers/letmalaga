<?php $this->load->view('panel/partials/header'); ?>
<div class="row justify-content-center">
  <div class="col-lg-7">
    <div class="card">
      <div class="card-header header-elements-inline">
        <h5 class="card-title">Preguntas frecuentes</h5>
        <div class="header-elements">
          <a href="<?=base_url('panel/faqs/create')?>" class="btn bg-brown-300"> <i class="icon-plus3 mr-2"></i>Crear una nueva Pregunta</a>
        </div>
      </div>

      <table class="table datatable-basic">
        <thead>
          <tr>
            <th>Pregunta</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          
          <?php foreach ($faqs as $faq): ?>
            <tr>
              <td><a href="<?=base_url('panel/faqs/edit/'.$faq->id)?>"><?=$faq->answer?></a></td>
              <td class="text-center">
                <div class="list-icons">
                  <div class="dropdown">
                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                      <i class="icon-menu9"></i>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                      <a href="<?=base_url('panel/faqs/edit/'.$faq->id)?>" class="dropdown-item"><i class="icon-pencil"></i> Editar</a>
                      <a href="<?=base_url('panel/faqs/delete/'.$faq->id)?>" class="dropdown-item"><i class="icon-trash"></i> Eliminar</a>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
</div>



<?php $this->load->view('panel/partials/footer'); ?>