<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/assets/css/layout.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/assets/css/components.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('template')?>/assets/css/colors.min.css" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  

</head>

<body>

  <!-- Page content -->
  <div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

      <!-- Content area -->
      <div class="content d-flex justify-content-center align-items-center">

        <!-- Login card -->
        <form class="login-form axios-client" action="panel/auth" method="post">
          <div class="card mb-0">
            <div class="card-body">
              <div class="text-center mb-3">
                <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                <h5 class="mb-0">Entra al Panel de control</h5>
                <span class="d-block text-muted">Tus credenciales</span>
              </div>

              <div class="form-group form-group-feedback form-group-feedback-left">
                <input type="text" class="form-control" placeholder="Email" name="email">
                <div class="form-control-feedback">
                  <i class="icon-user text-muted"></i>
                </div>
                <span class="form-text text-danger validation" id="validate-email"></span>
              </div>

              <div class="form-group form-group-feedback form-group-feedback-left">
                <input type="password" class="form-control" placeholder="Contraseña" name="password">
                <div class="form-control-feedback">
                  <i class="icon-lock2 text-muted"></i>
                </div>
                <span class="form-text text-danger validation" id="validate-password"></span>
              </div>

              <div class="form-group d-flex align-items-center">
                <a href="#" class="ml-auto">Forgot password?</a>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Inicia sesion <i class="icon-circle-right2 ml-2"></i></button>
              </div>             

              <span class="form-text text-center text-muted">Al continuar, confirma que ha leído nuestros <a href="#"> Términos & amp; Condiciones </a> y <a href="#"> Política de cookies</a></span>
            </div>
          </div>
        </form>
        <!-- /login card -->

      </div>
      <!-- /content area -->

    </div>
    <!-- /main content -->

  </div>
  <!-- /page content -->

  <!-- Core JS files -->
  <script src="<?=base_url('template')?>/global_assets/js/main/jquery.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/main/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/loaders/blockui.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/ui/ripple.min.js"></script>
  <!-- /core JS files -->


  <script src="<?=base_url('template')?>/global_assets/js/plugins/loaders/progressbar.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/notifications/jgrowl.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/notifications/noty.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/ui/fab.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/ui/sticky.min.js"></script>
  <script src="<?=base_url('template')?>/global_assets/js/plugins/ui/prism.min.js"></script>

  <script src="<?=base_url('node_modules')?>/axios/dist/axios.min.js"></script>
  <script src="<?=base_url('public')?>/js/axiosClient.js"></script>

  <script src="<?=base_url('template')?>/assets/js/app.js"></script>





</body>

</html>
