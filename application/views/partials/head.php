<header id="header">
  <div class="container">
    <div class="row justify-content-between align-items-center">
      <div class="col-6 col-lg-2 col-md-5 col-sm-5">
        <div id="logo">
          <a href="<?=base_url()?>">
            <?php if (!isset($background) or !$background): ?>
              <img src="<?=base_url('public')?>/images/logo-light.png" alt="LetMalaga" class="img-fluid">
            <?php else: ?>
              <img src="<?=base_url('public')?>/images/logo.png" alt="LetMalaga" class="img-fluid">
            <?php endif ?>
            
          </a>
        </div>
      </div>
      <div class="col-6 col-lg-10 col-md-7 col-sm-7">
        <nav id="navbar">
          <div class="hamburger hamburger--collapse" tabindex="0"
             aria-label="Menu" role="button" aria-controls="navigation">
            <div class="hamburger-box">
              <div class="hamburger-inner"></div>
            </div>
          </div>

          <ul class="navbar-list">
            <li class="navbar-item"><a href="<?=base_url('properties')?>">Propiedades</a></li>
            <li class="navbar-item"><a href="<?=base_url('services')?>">Servicios</a></li>
            <li class="navbar-item"><a href="<?=base_url('about')?>">Quiénes somos</a></li>
            <li class="navbar-item"><a href="<?=base_url('blog')?>">Blog</a></li>
            <li class="navbar-item"><a href="<?=base_url('contact')?>">Contacto</a></li>
            <li class="navbar-item"><a href="<?=base_url('panel')?>">Panel de control</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>