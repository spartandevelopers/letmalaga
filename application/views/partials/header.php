<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Letmalaga</title>
  <link rel="stylesheet" href="<?=base_url()?>/node_modules/@fortawesome/fontawesome-free/css/all.css">
  
  <link rel="stylesheet" href="<?=base_url()?>/node_modules/reset-css/reset.css">
  <link rel="stylesheet" href="<?=base_url()?>/node_modules/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>/node_modules/hamburgers/dist/hamburgers.min.css">
  <link rel="stylesheet" href="<?=base_url()?>/node_modules/lightgallery/dist/css/lightgallery.min.css">
  <link rel="stylesheet" href="<?=base_url()?>/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" />
  <link rel="stylesheet" href="<?=base_url('public')?>/fonts/stylesheet.css">
  <link rel="stylesheet" href="<?=base_url('public')?>/icons/flaticon.css">
  <link href="<?=base_url('template')?>/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  

  <link rel="stylesheet" href="<?=base_url()?>/node_modules/flatpickr/dist/flatpickr.min.css">
  <link rel="stylesheet" href="<?=base_url()?>/node_modules/%40material/checkbox/dist/mdc.checkbox.min.css">
  <link rel="stylesheet" href="<?=base_url('public')?>/css/all.css">

</head>
<body >
  <div id="loader"></div>
  <?php if (!isset($background) or !$background): ?>
    <?php $this->load->view('partials/head'); ?>
  <?php endif ?>
  