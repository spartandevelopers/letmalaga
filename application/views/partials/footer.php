
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="footer-title">Letmalaga</div>
          <div class="let-malaga-info">
            
            <ul class="contact-info">
              <li>
                <div class="icon"><i class="flaticon-message"></i></div>
                <div class="info">hello@letmalaga.com</div>
              </li>
              
              <li>
                <div class="icon"><i class="flaticon-phone-call"></i></div>
                <div class="info">+34 663 325 793</div>
              </li>

              <li>
                <div class="icon"><i class="flaticon-phone-call"></i></div>
                <div class="info">+34 951 493 932</div>
              </li>

              <li>
                <div class="icon"><i class="flaticon-skype-logo"></i></div>
                <div class="info">hello@letmalaga.com</div>
              </li>
            </ul>
           
          </div>
        </div>

        <div class="col-md-4">
          <div class="footer-title">Nosotros</div>
          <ul class="link-group">
            <li><a href="<?=base_url('about')?>">Quienes somos</a></li>
            <li><a href="<?=base_url('faqs')?>">Preguntas frecuentes</a></li>
            <li><a href="<?=base_url('privacity')?>">Política de privacidad</a></li>
            <li><a href="<?=base_url('cookies')?>">Política de cookies</a></li>
            <li><a href="<?=base_url('contact')?>">Contacto</a></li>
            <li><a href="<?=base_url('blog')?>">Blog</a></li>
          </ul>
        </div>
        <div class="col-md-4">
          <div class="let-malaga-info">
            <div class="logo">
              <img src="<?=base_url('public')?>/images/logo-light.png" alt="LetMalaga" class="img-fluid">
            </div>
            
            <div class="social">
              <div class="icons">
                <a href="#" class="icon facebook-color"><i class="laticon-facebook-logo-button"></i></a>
                <a href="#" class="icon twitter-color"><i class="flaticon-twitter-logo-button"></i></a>
                <a href="#" class="icon pinterest-color"><i class="flaticon-pinterest"></i></a>
                <a href="#" class="icon instagram-color"><i class="flaticon-instagram-logo"></i></a>
                <a href="#" class="icon youtube-color"><i class="flaticon-youtube"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center align-items-center">
        <div class="col-auto">
          <span class="bold text-white">2018 - Programado por <a href="ceroides.es" target="_blank" class="text-white">Cero Ideas</a></span>
        </div>
      </div>
    </div>
  </footer>

  <?php if (!$this->session->cookies): ?>
    <div class="avise-cookies">
      <div class="container">
        <div class="row">
          <div class="content">
            <span>
              Esta web utiliza cookies. Si continúas navegando consideramos que aceptas su uso. Puedes obtener más información en nuestra <a href="<?=base_url('cookies')?>">Política de Cookies</a>
            </span>
            <div class="actions">
              <span><div class="flaticon-down-arrow"></div></span>
              <ul>
                <li><a href="#" class="send-cookies">Acepto</a></li>
                <li><a href="#">No acepto</a></li>
              </ul>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  <?php endif ?>

  

  <script src="<?=base_url()?>/node_modules/jquery/dist/jquery.min.js"></script>
  <script src="<?=base_url()?>/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?=base_url()?>/node_modules/flatpickr/dist/flatpickr.min.js"></script>
  <script src="<?=base_url()?>/node_modules/lightgallery/dist/js/lightgallery.min.js"></script>
  <script src="<?=base_url()?>/node_modules/%40material/select/dist/mdc.select.min.js"></script>
  <script src="<?=base_url()?>/node_modules/owl.carousel/dist/owl.carousel.min.js"></script>

  <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"></script>
  <script src="<?=base_url('public')?>/js/languajes/es.js"></script>
  <script src="<?=base_url('public')?>/js/all.js"></script>
</body>
</html>

<script>
  $('.send-cookies').on('click', function() {
    $.ajax({
      url: '<?=base_url('accept_cookie')?>',
      type: 'get',
      dataType: 'json',
      suucess: () => {
        $('.avise-cookies').css('display','none');
      }
    })
    
  });
</script>